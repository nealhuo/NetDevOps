class Test(object):
    def __init__(self, world):
        self.world = world
    def __str__(self):
        return 'This is __str__ magic function'
    def __repr__(self):
        return 'This is __repr__ magic function'

t = Test('nealhuo')
print(t)  # print一个class的时候调用的是__str__这个魔法方法
print(Test('nealhuo')) # 同上
print(str(t))# 同上

print(repr(t))