'''
OPP重点：
1. 每一个class对必须有一个object，object是万类之母
2. 类中的每个方法都需要有self，因为绝大多数类中的方法都是要先实例化后再调用的，而self就代表实例化后的对象本身！
3. 子类继承父类的时候，如果子类没写 init 方法，默认就使用父类的init方法，但如果子类写了，那么默认覆盖父类的init。
   但除了子类的init方法，还想用父类的init方法，就要在子类的init中手动调用父类的init，如下例子：
'''
'''
class Router(object):
    def __init__(self, hostname, address, product):
        self.hostname = hostname
        self.address = address
        self.prodcut = product
    def update_add(self, newadd):
        self.address = newadd
        


class Junos(Router):
    def __init__(self, hostname, address, product, vendor):
        Router.__init__(self, hostname, address, product)
        self.vendor = vendor
    def update_product(self, newpro):
        self.prodcut = newpro
        

client = Junos('MX1','','','Juniper.Inc')
'''
########################################################################

class Router(object):
    def __init__(self, hostname, address, product):
        self.hostname = hostname
        self.address = address
        self.product = product
    def update_add(self, newadd):
        self.address = newadd
        


class Junos(Router):
    def update_product(self, newpro):
        self.prodcuts = newpro
        

client = Junos('MX1','1.1.1.1','JuniperInc')
print(client.hostname)
print(client.address)
print(client.product)
client.update_product('cisco')
print(client.prodcuts)

