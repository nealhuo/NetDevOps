
class CapStr(str):
    '''
    实际工作中很少重写new方法，但有一种情况下需要重写：当你继承一个不可变类型的时，又需要进行修改的时候，就需要重写了
    例如：定义一个类，继续str类，str类是无法修改的，只能重写__new__，并且把已经大写的字符传过去
    '''
    def __new__(cls, string):
        string = string.upper()
        return str.__new__(cls, string)  # 这里用str的new方法构造一个实例化后的对象，传入NEALHUO

c = CapStr('nealhuo')  #相当于 c = str('NEALHUO')
print(c)
