# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 00:57:01 2019

@author: nealhuo
"""
class Pagenation():
    def __init__(self, page):
        self.page = int(page)
    
    @property
    def start_page(self):
        start = (self.page)*10
        return start
    @property
    def end_page(self):
        end = (self.page+1)*10
        return end
        
l = range(1,1000)

while 1:
    num = input('Page Num: ')
    p = Pagenation(num)        
    print(l[p.start_page:p.end_page]) #感觉就像调用一个字段一样，看起来舒服
    


        

