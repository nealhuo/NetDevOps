class Test(object):
    def __init__(self, world):
        self.world = world
x = Test('world')
p = Test('world')
# print(hash(x))
# print (hash(x) == hash(p))
# print (hash(x.world) == hash(p.world))
'''
hash()方法总是会返回int型的数字。可以用于比较一个唯一的对象，比方说一个不同内存的object不会相当，
而相同字符串hash之后就会相等。然后我们通过修改__hash__方法来修改hash函数的行为。让他总是返回1241，也是可以轻松做到的。
'''
class Test2(object):
    '''
    help doc 这段字符用help(Test2)的时候可以看到
    '''
    def __init__(self, song):
        self.song = song
    def __hash__(self):
        return 1241
    def __dir__(self):
        # 如果不定义__dir__，默认就会返回所有的内置和非内置方法
        return ['func1', 'func2']
    def handle(self):
        pass

x = Test2('popo')
# dir的用方法如下，三种用法都一样，最后一种用法最不好看，必须随便传个参数，一点儿都不pythonic
print('__dir__ : ',x.__dir__())
print('__dir__ : ',dir(x))
print('__dir__ : ',Test2.__dir__('a'))
p = Test2('janan')
print (x, hash(x))
print (p, hash(p))
print(hash(x) == hash(p))


