class Test(object):
    '''
    对于每一个类的成员而言都有两种形式：
• 公有成员，在任何地方都能访问
• 私有成员，只有在类的内部才能方法
私有成员和公有成员的定义不同：私有成员命名时，前两个字符是下划线。（特殊成员除外，例如：__init__、__call__、__dict__等）
    '''
    username = 'nealhuo'
    __username = 'NEALHUO'
    def __init__(self):
        self.name='公有字段'
        self.__name='私有字段'
    def get_private_name(self):
        return self.__name, Test.__username  #
t = Test()

# 公有字段通过对象，类都能直接访问
print(t.name)
print(t.username)

# 私有字段，无法通过 对象，类 直接访问
# print(Test.__name) AttributeError: type object 'Test' has no attribute '__name'
# print(t.__name) # AttributeError: 'Test' object has no attribute '__fname'
# print(t.__username) AttributeError: 'Test' object has no attribute '__username'
print(t.get_private_name())  # 只有通过类内部来访问

