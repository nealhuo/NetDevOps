import redis

# r = redis.Redis(host='127.0.0.1', port=6379)
# r.set('foo','bar')  # 默认情况下，每次执行r.set会连接redis，操作完成后断开
# r.set('sex', 'man')
# print(r.get('se'))

'''
用连接池的情况下，可以使用管道进行批量操作：
redis-py默认在执行每次请求都会创建（连接池申请连接）和断开（归还连接池）一次连接操作，如果想要在一次请求中指定多个命令，则可以使用pipline实现一次请求指定多个命令
'''

pool = redis.ConnectionPool(host='127.0.0.1', port=6379)
r = redis.Redis(connection_pool=pool)
pipe = r.pipeline(transaction=True)  # 事务性为True，即，所有的pipe操作，要么都成功，要么都失败
pipe.set('name', 'alex')
pipe.set('role', 'sb')
print(pipe.get('name'))
print(pipe.get('role'))
pipe.execute()





