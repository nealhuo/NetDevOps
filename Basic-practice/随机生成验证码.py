# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 00:23:42 2019

@author: nealhuo
"""

import random

'''
要求共6位至少有一位是数字，一位是特殊符号
'''
str_pool = 'asdfghjklzxcvbn'
num_pool = '1234567'
symbol_pool = '@#$%^&*()'

def gen_code():
    code = ''
    for i in range(4):
       code += random.choice(str_pool)

    num = random.choice(num_pool)
    symbol = random.choice(symbol_pool)
    
    code = code + num +symbol
    code = list(code)
    random.shuffle(code) #shuffle方法会随机打乱列表顺序
    code = ''.join(code)
    return code
        
if __name__ == '__main__':
    print(gen_code())