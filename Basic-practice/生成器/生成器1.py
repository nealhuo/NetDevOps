# -*- coding: utf-8 -*-
"""
Created on Thu May  9 22:00:44 2019

@author: nealhuo
"""

g = (i*2  for i in range(10))  # 定义一个生成器
print(type(g))  
print(next(g))  # 生成器内置方法next（）
print(next(g))
print(next(g))
print(next(g))
print(next(g))