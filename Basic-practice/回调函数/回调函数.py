# -*- coding: utf-8 -*-
"""
Created on Thu May  9 22:28:25 2019

@author: nealhuo
"""
# 定义回调函数1
def double(x):
    return x * 2

# 定义回调函数2
def quadruple(x):
    return x*4

# 定义中间函数
def getOddNum(k, getEventNumber):
    return 1 + getEventNumber(k)

def main():
    # 当需要一个2k+1的基数时候   
    k = 1
    i = getOddNum(k, double) # 重点： Why not use double(k)+1 ?? 
    '''
    因为要通过登记不同的回调函数来决定中间函数的行为！
    通过传入回调函数double来让中间函数getOddNum的返回值表文2k+1
    通过传入回调函数quadruple来让中间函数getOddNum的返回值表文4k+1
    ''' 
    print(i)
    
    i = getOddNum(k, quadruple)
    print(i)
    
    i = getOddNum(k, lambda x: x*8)
    print(i)
    
main()
