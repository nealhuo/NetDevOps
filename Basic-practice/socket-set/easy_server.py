# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 17:29:52 2019

@author: nealhuo
"""

import socket
host = '' #表示监听 0.0.0.0
port = 12345
addr = (host, port)
s = socket.socket()  #实例化一个socket，参数看init方法就行了
#如果不加下面这行，系统默认保留套接字60s，期间无法再运行
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(addr)
s.listen(1)
cli_sock, soc_addr = s.accept()
print('Client connected from: {}'.format(soc_addr))
cli_sock.recv(1024)
cli_sock.send(b'I see U\r\n')
cli_sock.close()
s.close()
#telnet 127.0.0.1 12345 用于测试