# -*- coding: utf-8 -*-
"""
Created on Sun Apr 21 13:01:32 2019

@author: nealhuo
"""

import socket

host = '127.0.0.1'
port = 12345
addr = (host, port)
c = socket.socket()
c.connect(addr)
while 1:
    data = input('>')
    sdata = '{}\r\n'.format(data)
    c.send(sdata.encode('utf-8'))
    if sdata.strip() == 'q':
        break
    rdata = c.recv(1024)
    print(rdata.decode('utf-8'))
    
