# -*- coding: utf-8 -*-
"""
Created on Sat Mar  2 19:36:49 2019

@author: nealhuo
"""

def add(x):
    return x*x

a = [add(x) for x in range(5)]
print(a)