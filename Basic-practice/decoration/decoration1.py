# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 17:29:52 2019

@author: nealhuo
"""
import time

def timetest(func):
    def inner():
        stime = time.time()
        func()
        etime = time.time()
        
        return etime-stime
    return inner

def test():
    ''''''
    time.sleep(3)
    
if __name__ == '__main__':
    a = timetest(test)   #b =inner
    b = a() # a = inner()
    print(b)
    
    
