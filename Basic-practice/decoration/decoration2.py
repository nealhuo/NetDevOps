# -*- coding: utf-8 -*-
"""
Created on Sun Apr 28 17:07:14 2019

@author: nealhuo
"""

import time
# 闭包：如果在一个内部函数里，对在外部作用域（但不是在全局作用域）的变量进行引用，那么内部函数就被认为是闭包(closure)
# 被认为是闭包函数的三个条件：
# 1. 2. 3.
# 所以，装饰器就是闭包函数
def timetest(func):# 1. 函数能嵌套另外一个函数
    def inner():
        stime = time.time()
        func() # 2. 内部函数引用了外部函数的变量，这是规定死的
        etime = time.time()
        
        return etime-stime
    return inner # 3. 外部函数返回内嵌函数

@timetest  # 这时候test函数 就相当于 timetest(test)
def test():
    ''''''
    time.sleep(3)
    return 'origin func'
    
if __name__ == '__main__':
    # test = # timetest(test)
    # test()  =  timetest(test)()
    print(timetest(test)())   
    print(test()) 
    # 所以，装饰器是给函数插入新功能用的