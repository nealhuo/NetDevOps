
import asyncio

@asyncio.coroutine
def foo(ip):
    print('starting to connect {}'.format(ip))
    yield from asyncio.sleep(2)
    print('{} done'.format(ip))

ip_list = ['1.1.1.1', '2.2.2.2']


def test_do_something():
    # 用列表解析表达式产生多个协程对象放到list中
    task = [foo(ip) for ip in ip_list]
    print(task)
    # 获取一个事件循环对象
    loop = asyncio.get_event_loop()
    # 在事件循环中执行task列表
    loop.run_until_complete(asyncio.wait(task))
    # loop.run_until_complete(asyncio.gather(foo(ip1),foo(ip2)))
    loop.close()

test_do_something()



