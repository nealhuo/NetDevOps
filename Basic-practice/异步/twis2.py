from twisted.web.client import getPage # getpage用于发请求
from twisted.internet import reactor # reactor 用于事件循环

'''
利用twisted封装一个异步爬虫模块
'''
class TwistedRequest(object):
    def __init__(self):
        self.recv_counter = 0
        self.req_counter = 0

    def __execute(self, content, url, callback):
        if callback:
            callback(url, content)
        self.recv_counter += 1
        if self.recv_counter == self.req_counter:
            reactor.stop()

    def fetch_url(self, url_callback_list):
        self.req_counter = len(url_callback_list)
        for item in url_callback_list:
            url = item['url']
            success_callback = item['success_callback']
            error_callback = item['error_callback']
            deferred = getPage(bytes(url, encoding='utf8'))
            deferred.addCallback(self.__execute, url, success_callback)
            deferred.addErrback(self.__execute, url, error_callback)
        reactor.run()

def callback(url, content):
    print(url, content)

def error(url, content):
    print(url, content)


obj = TwistedRequest()

obj.fetch_url(
    [{'url': 'http://www.baidu.com', 'success_callback': callback, 'error_callback': error},
    {'url': 'http://www.google.com', 'success_callback': callback, 'error_callback': error}]
)