
'''
既然asyncio 不支持http，那么我就用socket写一个http不就行了.
线程执行顺序弄清楚：


'''

import asyncio


@asyncio.coroutine
def fetch_async(host, url='/'):
    print('starting to fetch', host, url)
    reader, writer = yield from asyncio.open_connection(host, 80)# socket 的io请求会阻塞，所以这里要yield一下

    request_header_content = """GET %s HTTP/1.0\r\nHost: %s\r\n\r\n""" % (url, host,)
    # 构造一个http请求头：GET %s HTTP/1.0 是http请求行， Host: %s 是http头， host字段是必选的
    request_header_content = bytes(request_header_content, encoding='utf-8')
    # 相当于 request_header_content = request_header_content.encode('utf-8')
    writer.write(request_header_content)
    print('starting to send', host, url)
    yield from writer.drain()
    print('starting to read', host, url)
    text = yield from reader.read()
    print(host, url, text)
    writer.close()

tasks = [
    fetch_async('www.cnblogs.com', '/wupeiqi/'),
    fetch_async('dig.chouti.com', '/pic/show?nid=4073644713430508&lid=10273091')
]

loop = asyncio.get_event_loop()
results = loop.run_until_complete(asyncio.gather(*tasks))
loop.close()
