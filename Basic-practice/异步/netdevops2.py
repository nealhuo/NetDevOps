
import asyncio
'''
用asyncio提供的@asyncio.coroutine可以把一个generator标记为coroutine类型，然后在coroutine内部用yield from调用另一个coroutine实现异步操作。

为了简化并更好地标识异步IO，从Python 3.5开始引入了新的语法async和await，可以让coroutine的代码更简洁易读。

请注意，async和await是针对coroutine的新语法，要使用新的语法，只需要做两步简单的替换：

把@asyncio.coroutine替换为async；
把yield from替换为await。
'''
async def foo(ip):
    print('starting to connect {}'.format(ip))
    await asyncio.sleep(2)
    print('{} done'.format(ip))

ip_list = ['1.1.1.1', '2.2.2.2']


def test_do_something():
    # 用列表解析表达式产生多个协程对象放到list中
    task = [foo(ip) for ip in ip_list]
    print(task)
    # 获取一个事件循环对象
    loop = asyncio.get_event_loop()
    # 在事件循环中执行task列表
    loop.run_until_complete(asyncio.wait(task))
    # loop.run_until_complete(asyncio.gather(foo(ip1),foo(ip2)))
    loop.close()

test_do_something()



