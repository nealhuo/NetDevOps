import asyncio
import time


'''
#py3.4之前用的是这种写法

两个套路：
1. 在需要异步的方法前加装饰器 @asyncio.coroutine
2. 事件循环
    loop = asyncio.get_event_loop()  #事件循环
    loop.run_until_complete(asyncio.gather(*task))
    loop.close()

缺点：不支持http

'''


@asyncio.coroutine
def func1():
    print('starting func1...')
    yield from asyncio.sleep(5)  #这里必须写 yield from asyncio.sleep， 表示异步IO执行5s。不能写sleep
    print('end func1...')

task = [func1(), func1()]  #有两个任务

loop = asyncio.get_event_loop()  #事件循环
loop.run_until_complete(asyncio.gather(*task))
loop.close()


