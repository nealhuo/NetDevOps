from twisted.web.client import getPage # getpage用于发请求
from twisted.internet import reactor # reactor 用于事件循环

rev_counter = 0  # 记录接受的个数
req_counter = 0  # 记录请求的个数

def callback(contents):
    print(contents)

    global rev_counter
    rev_counter += 1
    if rev_counter == req_counter:
        reactor.stop()


url_list = ['http://www.bing.com', 'http://www.baidu.com', ]
req_counter = len(url_list)
for url in url_list:
    print('starting to fetch ...', url)
    deferred = getPage(bytes(url, encoding='utf8'))
    print('Already to send requests...', url)
    deferred.addCallback(callback)

reactor.run()

