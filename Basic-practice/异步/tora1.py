from tornado.httpclient import AsyncHTTPClient  #tornado中的一个异步http客户端模块
from tornado.httpclient import HTTPRequest
from tornado import ioloop




url_list = [
        'http://www.baidu.com',
        'http://www.bing.com',
        'http://www.google.com',
    ]

req_counter = len(url_list)
rec_counter = 0


def handle_response(response):
    """
    处理返回值内容（需要维护计数器，来停止IO循环），调用 ioloop.IOLoop.current().stop()
    :param response:
    :return:
    """
    if response.error:
        print("Error:", response.error)
    else:
        print(response.body)
    global rec_counter
    rec_counter += 1
    if rec_counter == req_counter:
        ioloop.IOLoop.current().stop()

def func():
    for url in url_list:
        print(url)
        http_client = AsyncHTTPClient()  # 发请求
        http_client.fetch(HTTPRequest(url), handle_response) # 让程序有结果的时候执行这个handle这个回调函数就行了


ioloop.IOLoop.current().add_callback(func)  # 拿到一个循环，直接执行func
ioloop.IOLoop.current().start()