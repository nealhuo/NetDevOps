import time

class Fcelery(object):
    def __init__(self):
        self.broker = list()
        self.backend = dict()

    def reader(self, func):  # put user request into broker
        def inner():
            print('staring to append to broker')
            self.broker.append(func)
            print('endup append, {}'.format(self.broker))
            return func
        return inner

    def worker(self):
        while 1:
            print(self.broker)
            time.sleep(1)
            if self.broker:
                for func in self.broker:
                    res = func()
                    self.backend[func] = res
                    self.broker.remove(func)
                    print('task {} has finished'.format(func))
                    print('The backend is {}'.format(self.backend))

    def get_res(self, func):
        if self.backend.get(func):
            res = self.backend.get(func)
            self.backend.pop(func)
            print('Get {}: Success, The backend is {}'.format(func, self.backend))
            return res
        else:
            print('Get{}: Fail, The backend is {}'.format(func, self.backend))
            return 'Please wait the result.....'

app = Fcelery()
app.worker()