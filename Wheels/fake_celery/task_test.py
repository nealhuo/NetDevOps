from Fcelery import app
import time


@app.reader
def foo1():
    print('starting foo1')
    time.sleep(10)
    print('end up foo1')
    return 10

if __name__ == '__main__':
    addr = foo1()
    print('Got task id is {}', addr)
    print('ready to start worker~')
    print('Befor worker started, try to get result that is {}'.format(app.get_res(addr)))
    # app.worker() #TODO 如果不执行worker，task.py执行完毕后，foo1的内存就被释放了....如果把内存地址放入redis/文件中呢？
    time.sleep(12)
    print('Befor worker started, try to get result that is {}'.format(app.get_res(addr)))
    print(app.broker, app.backend)
