import sys

line_elements = []

def main(new_line):

    global line_elements

    new_line = new_line.strip()

    # Pre-processing the new_line if the command is deprecated in JUNOS
    if '## Warning:' in new_line and 'is deprecated' in new_line:
        new_line = new_line.split("## Warning:")[0].strip()

    if new_line.endswith(";"):
        last_command = new_line.rstrip(';')
        print ('set ' + ''.join(line_elements) + last_command)

    elif new_line.endswith('}'):
        #Backstep to upper hierarchy
        line_elements = line_elements[:-1]

    elif new_line.endswith('{'):
        line_elements.append(new_line.rstrip("{"))

    else:
        #Ignore the lines which doesn't matched
        pass

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print ('>> Usage: config-set.py ' +'<filename> \n'\
        'You can also redirect the output to a file with "> <output-filename>"')
        exit()
    else:
        filename = sys.argv[1]
        try:
            fh = open(filename,'r')
            for line in fh:
                main(line)
            fh.close()
        except:
            print ('"'+filename+'"' + " can not be opened! Make sure it is exist!")