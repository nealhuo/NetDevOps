# -*- coding: utf-8 -*-

conflist = []
config = []
n = 1 # 用于debug行号

def deal_info(line):
    global conflist
    global n
    line = line.strip()
    if line.endswith('{'):
        line = line.rstrip('{')
        conflist.append(line)
    elif line.endswith(';'):
        lastcmd = line.rstrip(';')
        result = 'set ' + ''.join(conflist) + lastcmd + '\n'
        config.append(result)
    elif '## Warning:' and 'is deprecated' in line:
        lastcmd = line.split(';')
        result = 'set '+ ' '.join(conflist) + lastcmd[0] + '\n'
        config.append(result)
    elif line.endswith('}'):
        conflist = conflist[:-1]
    else:
        print('no match')
    n += 1

def gen_conf(dfname):
    with open(dfname, 'w') as dfobj:
        dfobj.writelines(config)
        
    
if __name__ == '__main__':
    sfname = input('which file do you want to converse?')
    dfname = sfname + '.set'
    with open(sfname) as sfobj:
        for line in sfobj:
            deal_info(line)
    gen_conf(dfname)
    
    
    
    
        
        
            

