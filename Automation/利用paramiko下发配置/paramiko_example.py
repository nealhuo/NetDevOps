# -*- coding: utf-8 -*-
"""
Created on Mon Dec 31 19:52:06 2018

@author: nealhuo
"""

# 上传文件
import os
import sys
import paramiko

t = paramiko.Transport(('172.16.0.19', 22))
t.connect(username='root', password='123456')
sftp = paramiko.SFTPClient.from_transport(t)
sftp.put('log.log', '/tmp/log.log')
t.close()

# 下载文件
import os,sys
import paramiko

t = paramiko.Transport(('172.16.0.19',22))
t.connect(username='root', password='123456')
sftp = paramiko.SFTPClient.from_transport(t)
sftp.get('/tmp/log.log', 'log2.log')
t.close()

#执行命令 - 密匙
import paramiko

private_key_path = '/root/.ssh/id_rsa'
key = paramiko.RSAKey.from_private_key_file(private_key_path)

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect('172.16.0.19', 22, 'root', key)

stdin, stdout, stderr = ssh.exec_command('df -hT')
print(stdout.read())
ssh.close()

#上传或下载文件 - 密匙
# 上传文件
import paramiko

pravie_key_path = '/root/.ssh/id_rsa'
key = paramiko.RSAKey.from_private_key_file(pravie_key_path)

t = paramiko.Transport(('172.16.0.19', 22))
t.connect(username='root', pkey=key)

sftp = paramiko.SFTPClient.from_transport(t)
sftp.put('log.log','/tmp/log.log')

t.close()

# 下载文件
import paramiko

pravie_key_path = '/root/.ssh/id_rsa'
key = paramiko.RSAKey.from_private_key_file(pravie_key_path)

t = paramiko.Transport(('172.16.0.19', 22))
t.connect(username='root', pkey=key)

sftp = paramiko.SFTPClient.from_transport(t)
sftp.get('/tmp/log.log', 'log3.log')

t.close()

