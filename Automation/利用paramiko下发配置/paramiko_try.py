#coding:utf-8
import paramiko
'''
基本用法
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect('3.3.3.3', 22, 'root', 'root123')
stdin, stdout, stderr = ssh.exec_command('cli')
print(stdout.read().decode('utf-8'))    # 以utf-8编码对结果进行解码
ssh.close()
'''
def shell(hostname, port, username, password):
    # 创建SSH对象
    ssh = paramiko.SSHClient()

    # 允许连接不在know_hosts文件中的主机
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # 连接服务器
    ssh.connect(hostname, port, username, password)

    # 执行命令
    while 1:
        cmd = input('Input cmd: ')
        if not cmd.strip():
            ssh.close()
            break
        stdin, stdout, stderr = ssh.exec_command(cmd)

        # 获取命令结果
        result = stdout.read()
        print (result)

    #return result

if __name__ == '__main__':
    shell(hostname='3.3.3.3', port=22, username='root', password='root123')
    #test()
    
    