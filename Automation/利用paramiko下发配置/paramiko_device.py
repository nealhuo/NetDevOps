# -*- coding: utf-8 -*-
"""
Created on Tue Jan  1 15:45:25 2019

@author: nealhuo
"""
import paramiko

class Device(object):
    
    def __init__(self, device_info):
        self.mngip = device_info.get('mngip', 'None')
        self.port = device_info.get('port', 'None')
        self.username = device_info.get('username', 'None')
        self.passwd = device_info.get('passwd', 'None')
    
    def connect(self):
        # 创建SSH对象
        client = paramiko.SSHClient()    
        # 允许连接不在know_hosts文件中的主机
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # 连接服务器
        client.connect(self.mngip, self.port, self.username, self.passwd)
        return client

class CiscoXR(Device): 
                  
    def get_config(self):
        client = CiscoXR.connect(self)        
        cmd = 'show running-config'        
        # 执行命令   
        stdin, stdout, stderr = client.exec_command(cmd)    
        # 获取命令结果
        result = stdout.read()
        result = result.decode(encoding='utf-8')
        client.close()
        print (result)
        return result

class Junos(Device):
    
    def get_config(self):
        client = CiscoXR.connect(self)        
        cmd = 'pwd'        
        # 执行命令   
        stdin, stdout, stderr = client.exec_command(cmd)    
        # 获取命令结果
        result = stdout.read()
        result = result.decode(encoding='utf-8')
        client.close()
        print (result)
        return result

if __name__ == '__main__':
    Cdevice_info = {'mngip':'1.1.1.1', 'port':'22', 'username':'cisco', 'passwd':'cisco'}
    Jdevice_info = {'mngip':'3.3.3.3', 'port':'22', 'username':'root', 'passwd':'root123'}
    xr1 = CiscoXR(Cdevice_info)
    xr1.get_config()
    vmx1 = Junos(Jdevice_info)
    vmx1.get_config()
    
    
    
    
    
    
    