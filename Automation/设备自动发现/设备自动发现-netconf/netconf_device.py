# -*- coding: utf-8 -*-
"""
Created on Tue Jan  1 19:16:50 2019

@author: nealhuo
"""

from ncclient import manager

def validate_connection(mng_ip, username, password, device_sys):
    device = {
        "host": mng_ip,
        "port": 22,
        "username": username,
        "password": password,
        "hostkey_verify": False,
        "device_params": {"name": device_sys}
    }
    try:
        nc = manager.connect(**device)
        return True, nc
    except Exception as e:
        # Error example: Could not open socket to 2.2.2.2:22
        log(e)
        error_hint = '添加设备{}失败，请检查网络连接/用户名/密码'.format(mng_ip)
        return False, error_hint


def get_basic_info(nc):
    soft_info = nc.rpc('<get-software-information></get-software-information>')
    lo0_info = nc.rpc(' <get-interface-information><terse/><interface-name>lo0.0</interface-name>'
                      '</get-interface-information>')

    mng_ip = lo0_info.xpath('//logical-interface/address-family[1]/interface-address/ifa-local/text()')
    hostname = soft_info.xpath('//host-name//text()')
    version = soft_info.xpath('//junos-version/text()')


    return mng_ip, hostname, version


if __name__ == '__main__':
    result, nc = validate_connection('3.3.3.3', 'root', 'root123', 'junos')
    l = get_basic_info(nc)
    print(l)
    print(l[1][0])