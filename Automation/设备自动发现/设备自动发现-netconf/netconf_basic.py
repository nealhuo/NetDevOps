#coding:utf-8
from ncclient import manager

device = {"host": "3.3.3.3", "port": 22, "username": "root",
          "password":"root123", "hostkey_verify": False,
          "device_params": {"name":"junos"}}
nc = manager.connect(**device)

config = nc.get_config(source='running')#获取设备所有配置
#print(type(config))
#print(config)
interfaces = nc.rpc("<get-interface-information><terse/></get-interface-information>")#获取接口配置
version = nc.rpc(" <get-software-information></get-software-information>")
'''
xpath 语法：
Nodename: 选取此节点所有的子节点
'''
# print type(interfaces.xpath)
print(type(version.xpath('//host-name')))
interfaces_lsit = []
for interface in interfaces.xpath('//name'):
    intf = interface.text
    intf = intf.strip()
    interfaces_lsit.append(intf)
#print (interfaces_lsit)
    
'''
总结：用netconf对设备操作/数据收集 其实并不复杂，只需要掌握操作层的一些方法就可以很容易的取出数据。而且ncclient
还集成了XML的处理，可以用xpath的语法来处理XML，非常方便。其实我们在处理网络设备信息时，大量的工作是在处理JSON和XML，
掌握多种JSON和XML的数据处理方式是很有必要的。
'''

