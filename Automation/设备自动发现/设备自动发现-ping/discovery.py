# -*- coding: utf-8 -*-
"""
Created on Tue Jan  1 17:56:23 2019

@author: nealhuo

"""
'''
通过多线程来实现设备自动发现,需要6s多，缺点：时间过长
'''
import subprocess as sp
import threading
import time

iplist = ['1.1.1.1', '2.2.2.2', '3.3.3.3', '4.4.4.4', '5.5.5.5',
          '6.1.1.1', '7.2.2.2', '8.3.3.3', '9.4.4.4', '10.5.5.5']
unabled_ip_list = []

def ping(ip):
    exitcode, result = sp.getstatusoutput('ping -n 1 -w 300 %s' % ip)
    if 'Request timed out.' in result:
         unabled_ip_list.append(ip)
    else:
        pass
    return exitcode, result

def discovery():
    for ip in iplist:
        th = threading.Thread(target=ping, args=[ip])
        th.start()
        th.join() # 用于阻塞，告诉程序，th不结束，谁也别往下走
        
print (time.ctime())        
discovery()
print (unabled_ip_list)
print (time.ctime())  

    
'''
++++++++++++++++++经过测试，不用多线程的话，每10个ip就会多花一秒+++++++++++++++++++++++++
def ping2():
    for ip in iplist:
        exitcode, result = sp.getstatusoutput('ping -n 1 -w 300 %s' % ip)
    if 'Request timed out.' in result:
         unabled_ip_list.append(ip)
    else:
        pass
    return exitcode, result

print(time.ctime())
ping2()
print(time.ctime())
'''
