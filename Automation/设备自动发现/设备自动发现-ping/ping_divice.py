# -*- coding: utf-8 -*-
"""
Created on Tue Jan  1 17:56:23 2019

@author: nealhuo

"""

import subprocess as sp
import threading
import time

begin = time.time()
iplist = ['1.1.1.1', '2.2.2.2', '3.3.3.3', '4.4.4.4', '5.5.5.5',
          '6.1.1.1', '7.2.2.2', '8.3.3.3', '9.4.4.4', '10.5.5.5']
unabled_ip_list = []
def ping(ip):
    exitcode, result = sp.getstatusoutput('ping -n 1 -w 300 %s' % ip)
    if 'Request timed out.' in result:
         unabled_ip_list.append(ip)
    else:
        pass
    return exitcode, result

def discovery():
    for ip in iplist:
        th = threading.Thread(target=ping, args=[ip])
        th.start() #开始执行线程
        th.join()#挂起线程

   
    
        
        
#因为python全局锁的存在，所以python中的多线程并不是真正意义上的多线程，而是丢手绢式的处理
#所以需要join函数来挂起
        
     
discovery()
print (unabled_ip_list)
end = time.time()
t = end - begin
print(t)
'''
++++++++++++++++++经过测试，不用多线程的话，每10个ip就会多花 1 秒,省不了多少，甚至有的时候还会慢！+++++++++++++++++++++++++
def ping2():
    for ip in iplist:
        exitcode, result = sp.getstatusoutput('ping -n 1 -w 300 %s' % ip)
    if 'Request timed out.' in result:
         unabled_ip_list.append(ip)
    else:
        pass
    return exitcode, result

print(time.ctime())
ping2()
print(time.ctime())
'''
