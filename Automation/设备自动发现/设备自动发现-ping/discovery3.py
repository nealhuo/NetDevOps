"""
nealhuo 3/9/2019
"""
'''
启用线程池的概念，避免同时生成大量线程造成卡顿。例如4核PC，那我就一次用4个线程呗,平均时间1.82秒。
'''

import subprocess as sp
import time
from multiprocessing import Manager, Pool


def ping(ip, unabled_ip_list):
    exitcode, result = sp.getstatusoutput('ping -n 1 -w 300 %s' % ip)
    if 'Request timed out.' in result:
        unabled_ip_list.append(ip)
    else:
        pass
    return exitcode, result


if __name__ == '__main__':
    iplist = ['1.1.1.1', '2.2.2.2', '3.3.3.3', '4.4.4.4', '5.5.5.5',
              '6.1.1.1', '7.2.2.2', '8.3.3.3', '9.4.4.4', '10.5.5.5']
    start = time.time()
    pool = Pool(4)
    with Manager() as manger:
        unabled_ip_list = manger.list()
        for ip in iplist:
            pool.apply_async(func=ping, args=(ip, unabled_ip_list))
        pool.close()
        pool.join()  # 进程池中进程执行完毕后再关闭，如果注释，那么程序直接关闭。
        print(unabled_ip_list)
    end = time.time()
    print(end-start)