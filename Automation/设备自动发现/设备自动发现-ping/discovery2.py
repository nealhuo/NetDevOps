# -*- coding: utf-8 -*-
"""
Created on Tue Jan  1 17:56:23 2019

@author: nealhuo
"""
'''
换成多进程，放弃排序，只要1.59-1.9秒左右，缺点：如果ip过多，例如一个200个地址，就会瞬间增加200个进程，可能导致系统卡顿
'''
import subprocess as sp
import time
from multiprocessing import Process, Manager

iplist = ['1.1.1.1', '2.2.2.2', '3.3.3.3', '4.4.4.4', '5.5.5.5',
          '6.1.1.1', '7.2.2.2', '8.3.3.3', '9.4.4.4', '10.5.5.5']



def ping(ip, unabled_ip_list):
    exitcode, result = sp.getstatusoutput('ping -n 1 -w 300 %s' % ip)
    if 'Request timed out.' in result:
        unabled_ip_list.append(ip)
    else:
        pass
    return exitcode, result


def discovery():
    with Manager() as manger:
        unabled_ip_list = manger.list()
        for ip in iplist:
            p = Process(target=ping, args=[ip, unabled_ip_list,])
            p.start()

        p.join()  # 没必要按照顺去append进列表，所以就不用每个进程都join一下了。只需要最后一个进程join，告诉程序，最后一个进程没执行完谁也别往下走
        print(unabled_ip_list)


if __name__ == '__main__':
    start = time.time()
    result = discovery()
    # print(result)
    end = time.time()
    print(end-start)


