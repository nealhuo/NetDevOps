# -*- coding: utf-8 -*-
"""
Created on Wed Jan  9 20:49:06 2019

@author: nealhuo

通过snmp来取数据首先需要安装net-snmp，把本机做成一台snmp服务器，支持snmpwalk/get等基本命令，
再利用os.popen方法运行命令获取输出，针对输出进行处理。
"""

import os

def snmpWalk(host, community, oid):
    """利用os模块打开一个管道运行snmpwalk工具结合host，团体字符串，OID获取交换机路由器状态
    这里注意，对于非系统自带命令，哪怕设置了环境变量能够在cmd中执行，但使用popen方法时还需要
    指定路径，否则popen返回的结果就是一个空值    
    """
    result = os.popen(r'C:\usr\bin\snmpwalk.exe -v 2c -c ' + community + ' '+ host + ' ' + oid).read().split('\n')[:-1]
    return result

def getPortDevices(host,community):
    """获取端口信息"""
    device_mib = snmpWalk(host, community, 'RFC1213-MIB::ifDescr')
    device_list = []
    for item in device_mib:
        device_list.append(item.split(':')[3].strip())
    return device_list

def getPortStatus(host,community):
    """获取端口状态信息"""
    device_mib = snmpWalk(host, community, 'RFC1213-MIB::ifOperStatus')
    device_list = []
    for item in device_mib:
        device_list.append(item.split(':')[3].strip())
    return device_list

def main():
        host = input("Enter IP Address:")
        community =input("Enter String:")
        print('=' * 10 + host + '=' * 10)
        print("system info")
        DeviceList = getPortDevices(host,community)
        DeviceStatus = getPortStatus(host,community)
        for item in DeviceList:
                index = DeviceList.index(item)
                print("Port:"+item+" Status:"+DeviceStatus[index])

if __name__ == '__main__':
    main()


