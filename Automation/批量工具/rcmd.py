import paramiko

def rcmd(hostname, username, password, port=22):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())   # 允许连接不在known_hosts文件上的主机
    try:
        ssh.connect(hostname=hostname, port=port, username=username, password=password)# 连接服务器
        while 1:
            cmd = input('CMD: ')
            if cmd.strip() == 'end':
                print('End....')
                ssh.close()
                exit()
            else:
                stdin, stdout, stderr = ssh.exec_command(cmd) # ssh.exec_command会返回三个值，分别是标准输入，输出，错误，都是类文件对象，所以可以用read方法来读.但是stdin没有read这个方法！
                print('second....')
            # 获取结果
                data = stdout.read().decode()
                err = stderr.read().decode() # 获取错误提示（stdout、stderr只会输出其中一个）
                if data:
                    print('{}操作成功：{}'.format(host, data))
                else:
                    print('{}操作失败：{}'.format(host, err))
    except:
        ssh.close()


if __name__ == '__main__':
    hosts = ['192.168.30.129']
    for host in hosts:
        rcmd(host, 'root', 'eve')
