from multiprocessing import Process, Manager
import paramiko
import time


def rcmd(hostname, password, username, lcmd, log_obj):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh.connect(hostname=hostname, port=22, username=username, password=password)
    for cmd in lcmd:
        stdin, stdout, stderr = ssh.exec_command(cmd)
        output = stdout.read().decode()
        errput = stderr.read().decode()
        if output:
            print(output)
            log_obj.writelines([mngip + '\n', cmd + '\n', output + '\n', '*'*50 + '\n'])
        else:
            print(errput)
            log_obj.writelines([mngip + '\n', cmd + '\n', errput + '\n', '*'*50 + '\n'])
    ssh.close()



def get_device(fname):
    device = dict()
    with open(fname) as fobj:
        for line in fobj:
            line = line.strip()
            mngip = line.split()[0]
            username = line.split()[1]
            password = line.split()[2]
            device[mngip] = (username, password)
    return device

def get_cmd(fname):
    lcmd = list()
    with open(fname) as fobj:
        for line in fobj:
            cmd = line.strip()
            lcmd.append(cmd)
    return lcmd

def get_time():
    fmat = '%Y_%m_%d_%H_%M_%S'
    value = time.localtime(int(time.time()))
    dt = time.strftime(fmat, value)
    return dt

if __name__ == '__main__':
    log_time = get_time()
    logfname = 'log_{}.txt'.format(log_time)
    log_obj = open(logfname, 'a')
    device = get_device('device.txt')
    lcmd = get_cmd('cmd.txt')
    for mngip in device:
        username = device.get(mngip)[0]
        password = device.get(mngip)[1]
        rcmd(hostname=mngip, username=username, password=password, lcmd=lcmd, log_obj=log_obj)
        break
    log_obj.close()



