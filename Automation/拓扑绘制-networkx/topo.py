import networkx as nx

nodes = ['leaf1', 'leaf2', 'leaf3', 'Spine1', 'Spine2']
G = nx.Graph()
for node in nodes:
    G.add_node(node)

edges = [('leaf1', 'Spine1'),
         ('leaf1', 'Spine2'),
         ('leaf2', 'Spine1'),
         ('leaf2', 'Spine2'),
         ('leaf3', 'Spine1'),
         ('leaf3', 'Spine2'),
         ]
G.add_edges_from(edges)
