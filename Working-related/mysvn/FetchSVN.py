#!/usr/bin/python3
# coding:utf-8

import requests
import re
import argparse
import getpass
# from requests.packages.urllib3.exceptions import InsecureRequestWarning
# requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


def GetResult(url):
    requests.packages.urllib3.disable_warnings()
    """
    第一次请求svnmap,获取cookie PHPSESSID
    """
    userid=input("Please Enter your windows account:")
    password=getpass.getpass()
    header = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0"}
    Data = requests.get(url, headers=header, verify=False)
    PHPSession_ID = Data.cookies["PHPSESSID"]
    Keyword = re.compile('name="SAMLRequest" value="(.*?)"')
    SAMLRequest = Keyword.findall(Data.text)
    '''
    第二步向https://iam-sso.juniper.net/oamfed/idp/samlv20提交SAMLRequest
    '''
    url2 = "https://iam-sso.juniper.net/oamfed/idp/samlv20"
    header2 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url}
    Data2 = requests.post(url2, headers=header2, verify=False, data={"SAMLRequest": SAMLRequest, "RelaySate": url})
    cookies = requests.utils.dict_from_cookiejar(Data2.history[0].cookies)
    Updatecookies = requests.utils.dict_from_cookiejar(Data2.cookies)
    cookies.update(Updatecookies)

    """
    第三步向https://iam-sso.juniper.net/oam/server/auth_cred_submit发起POST请求
    """
    url3 = 'https://iam-sso.juniper.net/oam/server/auth_cred_submit'
    header3 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": Data2.url}
    Data3 = requests.post(url3, headers=header3, verify=False,
                          data={"HiddenURI": None, "LOCALE": "en_us", "AUTHMETHOD": "UserPassword", "userid": userid,
                                "password": password, "rememberme": "on"}, cookies=cookies)
    Keyword = re.compile('NAME="SAMLResponse" VALUE="(.*)"/>', re.DOTALL)
    SAMLResponse = Keyword.findall(Data3.text)
    """
    第四步向https://gubber.juniper.net/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp发起POST请求
    """
    url4 = 'https://gubber.juniper.net/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp'
    header4 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url3}
    Data4 = requests.post(url4, headers=header4, verify=False,
                          data={"SAMLResponse": SAMLResponse[0], "RelayState": url},
                          cookies={"PHPSESSID": PHPSession_ID})

    """
    获取所有结果
    """
    from bs4 import BeautifulSoup
    soup = BeautifulSoup(Data4.text, "html.parser")
    tables = soup.find("table", "bordered")
    sections = tables.find_all('tr')
    Records = []
    for i in range(1, len(sections)):
        record = sections[i].td.getText() + "  " + sections[i].b.getText()
        Records.append(record)

    """
    提取所有SVN-Says： 
    """
    TotalPR = len(Records)
    svn = 'SVN-Says:  YES'
    # pr_id='^PR \d+'
    result = []
    for record in Records:
        if svn in record:
            result += (re.compile('^PR (\d+)').findall(record))
    set(result)
    return result


def BulkCheck(list, version):
    """
    第一次请求svnmap,获取cookie PHPSESSID
    """
    userid=input("Please Enter your windows account")
    password=input("Password:")
    url = "https://gubber.juniper.net/svn-map/"
    header = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0"}
    Data = requests.get(url, headers=header, verify=False)
    PHPSession_ID = Data.cookies["PHPSESSID"]
    Keyword = re.compile('name="SAMLRequest" value="(.*?)"')
    SAMLRequest = Keyword.findall(Data.text)

    '''
    第二步向https://iam-sso.juniper.net/oamfed/idp/samlv20提交SAMLRequest
    '''
    url2 = "https://iam-sso.juniper.net/oamfed/idp/samlv20"
    header2 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url}
    Data2 = requests.post(url2, headers=header2, verify=False, data={"SAMLRequest": SAMLRequest, "RelaySate": url})
    cookies = requests.utils.dict_from_cookiejar(Data2.history[0].cookies)
    Updatecookies = requests.utils.dict_from_cookiejar(Data2.cookies)
    cookies.update(Updatecookies)

    """
    第三步向https://iam-sso.juniper.net/oam/server/auth_cred_submit发起POST请求
    """
    url3 = 'https://iam-sso.juniper.net/oam/server/auth_cred_submit'
    header3 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": Data2.url}
    Data3 = requests.post(url3, headers=header3, verify=False,
                          data={"HiddenURI": None, "LOCALE": "en_us", "AUTHMETHOD": "UserPassword", "userid": userid,
                                "password": password, "rememberme": "on"}, cookies=cookies)
    Keyword = re.compile('NAME="SAMLResponse" VALUE="(.*)"/>', re.DOTALL)
    SAMLResponse = Keyword.findall(Data3.text)
    """
    第四步向https://gubber.juniper.net/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp发起POST请求
    """
    url4 = 'https://gubber.juniper.net/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp'
    header4 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url3}
    Data4 = requests.post(url4, headers=header4, verify=False,
                          data={"SAMLResponse": SAMLResponse[0], "RelayState": url},
                          cookies={"PHPSESSID": PHPSession_ID})
    AuthToken = Data4.history[0].cookies["SimpleSAMLAuthToken"]
    """
    第五步 提交PRList
    """
    url5 = "https://gubber.juniper.net/svn-map/bulk_check_prs.php"
    header5 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url}
    Data5 = requests.post(url5, headers=header5, verify=False, data={"bug_ids": list, "repos": "", "ver_kws": version},
                          cookies={"PHPSESSID": PHPSession_ID, "SimpleSAMLAuthToken": AuthToken})
    print(Data5.text)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Interoperate with SVN Map.')
    parser.add_argument('options', help='BulkCheck submit PRs to SVN Map for check.'
                                        'Fetch Fetch    fetch the result from SVN Map', choices=["check", "fetch"])
    args = parser.parse_args()
    if args.options == "check":
        PRlist = input("Please enter PR Number separate by SPACE:")
        TargetVersion = input("Please enter the target version:")
        BulkCheck(PRlist, TargetVersion)
    else:
        ResultURL = input("Please provide URL for SVN Check result:")
        ResultList = GetResult(ResultURL)
        print("Below PRs are fixed in target version:")
        print(" ".join(ResultList))
        print("The total fixed PR Number is " + str(len(ResultList)))
