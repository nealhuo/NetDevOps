# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 10:57:26 2019

@author: nealhuo
"""
import requests
import re
from lxml import etree
from multiprocessing import Manager, Pool, freeze_support
import time
from openpyxl import load_workbook
import getpass

userinfo = dict()


def login_check(func):
    def inner():
        if userinfo:
            func(userinfo['userid'], userinfo['password'])
        else:
            userid = input('Please Enter your windows account: ')
            userid = userid.strip()
            password = getpass.getpass()
            password = password.strip()
            userinfo['userid'] = userid
            userinfo['password'] = password
            func(userinfo['userid'], userinfo['password'])
    return inner
'''
@login_check   # login_check(bulk_check)() = inner()
def bulk_check()
'''

def get_time():
    fmat = '%Y_%m_%d_%H_%M_%S'
    value = time.localtime(int(time.time()))
    dt = time.strftime(fmat, value)
    return dt

def single_check(url, usrid, password, bug_id, version, fixed_list, errorlist):
    requests.packages.urllib3.disable_warnings()
    header = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0"}
    Data = requests.get(url, headers=header, verify=False)
    PHPSession_ID = Data.cookies["PHPSESSID"]
    # print(Data.cookies['PHPSESSID']) # 获取session
    Keyword = re.compile('name="SAMLRequest" value="(.*?)"')  # (.*?)才是re表达式
    SAMLRequest = Keyword.findall(Data.text)  # 取出session
    # print(SAMLRequest)
    '''
        第二步向https://iam-sso.juniper.net/oamfed/idp/samlv20提交SAMLRequest
    '''
    url2 = "https://iam-sso.juniper.net/oamfed/idp/samlv20"
    header2 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url}
    Data2 = requests.post(url2, headers=header2, verify=False, data={"SAMLRequest": SAMLRequest, "RelaySate": url})
    cookies = requests.utils.dict_from_cookiejar(Data2.history[0].cookies)
    Updatecookies = requests.utils.dict_from_cookiejar(Data2.cookies)  # 将cookiejar转换为字典
    cookies.update(Updatecookies)  # 更新url2的cookie

    """
        第三步向https://iam-sso.juniper.net/oam/server/auth_cred_submit发起POST请求用于登录
    """
    url3 = 'https://iam-sso.juniper.net/oam/server/auth_cred_submit'
    header3 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": Data2.url}
    Data3 = requests.post(url3, headers=header3, verify=False,
                          data={"HiddenURI": None, "LOCALE": "en_us", "AUTHMETHOD": "UserPassword", "userid": usrid,
                                "password": password, "rememberme": "on"}, cookies=cookies)
    Keyword = re.compile('NAME="SAMLResponse" VALUE="(.*)"/>', re.DOTALL)
    SAMLResponse = Keyword.findall(Data3.text)
    """
        第四步向https://gubber.juniper.net/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp发起POST请求
    """
    url4 = 'https://gubber.juniper.net/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp'
    header4 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url3}
    Data4 = requests.post(url4, headers=header4, verify=False,
                          data={"SAMLResponse": SAMLResponse[0], "RelayState": url},
                          cookies={"PHPSESSID": PHPSession_ID})
    AuthToken = Data4.history[0].cookies["SimpleSAMLAuthToken"]

    url5 = 'https://gubber.juniper.net/svn-map/is_it_fixed.php?repos=&bug_id={}&branch_name={}'.format(bug_id, version)
    header5 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url}
    resp = requests.get(url5, headers=header5,
                        verify=False,
                        cookies={"PHPSESSID": PHPSession_ID, "SimpleSAMLAuthToken": AuthToken}
                        )

    content = resp.text
    html = etree.HTML(content)
    html_data = html.xpath('//*[@id="result"]/font')
    print(html_data)
    caution = html.xpath('//*[@id="result"]/b[1]/font')

    for text in caution:
        if 'CAUTION' in text.text:
            print('pr {} is Caution pr!!!'.format(bug_id))
            return None
    html_data_list = list(html_data)


    if (len(html_data_list)) == 0:
        print('{}: error, please check network , userinfo or target version...'.format(bug_id))
        errorlist.append(bug_id + 'need to manually chech' + '\n')
        # print(errorlist, 'sigle check')
        return None

    for r in html_data_list:
        if r.text:   # r.text maybe equal to None
            if r.text.strip() == 'YES':
                if bug_id not in fixed_list:
                    fixed_list.append(bug_id)
                    print('{}: fixed'.format(bug_id))
                    break

    if bug_id not in fixed_list:
        print('{}: not-fixed'.format(bug_id))

    return None

@login_check
def bulk_check(usrid, password):
    url = 'https://gubber.juniper.net/svn-map/'
    version = input('Please enter version: ')
    version = version.strip()
    print('Please enter PRs Number, Enter q when you are finished')
    bug_ids = list()
    while 1:
        bug_id = input()
        if bug_id.strip() == 'q':
            break
        bug_ids.append(bug_id)
    print('Starting to check......')
    stime = time.time()
    result_fname = 'Bcheck_result_{}.txt'.format(get_time())
    pool = Pool(5)
    with Manager() as manager:
        fixed_list = manager.list()
        errorlist = manager.list()
        for bug_id in bug_ids:
            t = pool.apply_async(func=single_check, args=(url, usrid, password, bug_id, version, fixed_list, errorlist))
        pool.close()
        pool.join()
        etime = time.time()
        write_list = list()
        for pr in fixed_list:
            write_list.append(pr + '\n')
        with open(result_fname, 'a') as robj:
            robj.writelines(write_list)
            print(errorlist, 'bulk check')
            robj.writelines(errorlist)
        # print('fixed_list: {}'.format(fixed_list))
        print('Done, {} PRs takes {}s'.format(len(bug_ids), etime-stime))
        print('Of the {} PRs, {} have been fixed, Please see in log for detail'.format(len(bug_ids), len(fixed_list)))


def get_jsa_mapping_list():
    # 默认可读写，若有需要可以指定write_only和read_only为True
    try:
        wb = load_workbook('jsa.xlsx')
    except FileNotFoundError as info:
        print('The "jsa.xlsx" cannot be found in the directory, please confirm it. ')
        menu()
    # 根据sheet名字获得sheet
    a_sheet = wb.get_sheet_by_name('Sheet')
    mapping_table = dict()
    for row in a_sheet.rows:
        if row[11].value:
            rv = row[11].value.strip()
            if rv:
                tracking_pr = rv.split(',')
                res = re.search(r'JSA\d+', row[2].value)
                try:
                    jsa_id = res.group()
                    mapping_table[jsa_id] = tracking_pr
                except AttributeError: # 正则取不到值
                    print(row[2].value)
    return mapping_table

def get_tsb_mapping_list():
    # 默认可读写，若有需要可以指定write_only和read_only为True
    try:
        wb = load_workbook('tsb_with_tag.xlsx')
    except FileNotFoundError as info:
        print('The "tsb_with_tag.xlsx" cannot be found in the directory, please confirm it. if the format is xls, It needs to be saved as xlsx. ')
        menu()
    # 根据sheet名字获得sheet
    a_sheet = wb.get_sheet_by_name('sheet')
    mapping_table = dict()
    for row in a_sheet.rows:
        v = row[10].value
        if v != ' ' and v != None:
            tracking_pr = row[10].value.split(',')
            res = re.search(r'TSB\d+', row[3].value)
            try:
                jsa_id = res.group()
                mapping_table[jsa_id] = tracking_pr
            except AttributeError: # 正则取不到值
                print(row[3].value)
    return mapping_table

def jsa_check(usrid, password, tracking_prs):
    url = 'https://gubber.juniper.net/svn-map/'
    version = input('Please enter version: ')
    version = version.strip()
    bug_ids = tracking_prs
    g_fixed_list = list()
    print('Starting to JSA check......')
    pool = Pool(5)
    with Manager() as manager:
        fixed_list_jsa = manager.list()
        errorlist = manager.list()
        for bug_id in bug_ids:
            t = pool.apply_async(func=single_check, args=(url, usrid, password, bug_id, version, fixed_list_jsa, errorlist))
        pool.close()
        pool.join()

        # print(fixed_list_jsa)
        for pr in fixed_list_jsa:
            g_fixed_list.append(pr)

        if errorlist:
            print('During the checking, error happens maybe because of network/username/password and so on. Please see pr errorlist file and then check it manualy')
            with open('JSA_errorlist_{}.txt'.format(get_time()), 'a') as robj:
                for i in errorlist:
                    robj.write(i + '\n')
    # print(g_fixed_list)
    return g_fixed_list


def tsb_check(usrid, password, tracking_prs):
    url = 'https://gubber.juniper.net/svn-map/'
    version = input('Please enter version: ')
    version = version.strip()
    bug_ids = tracking_prs
    g_fixed_list = list()
    print('Starting to TSB check......')
    pool = Pool(5)
    with Manager() as manager:
        fixed_list_tsb = manager.list()
        errorlist = manager.list()
        for bug_id in bug_ids:
            t = pool.apply_async(func=single_check, args=(url, usrid, password, bug_id, version, fixed_list_tsb, errorlist))
        pool.close()
        pool.join()

        print(fixed_list_tsb)
        for pr in fixed_list_tsb:
            g_fixed_list.append(pr)
        if errorlist:
            print('During the checking, error happens maybe because of network/username/password and so on. Please see pr errorlist file and then check it manualy')
            with open('TSB_errorlist_{}.txt'.format(get_time()), 'a') as robj:
                for i in errorlist:
                    robj.write(i + '\n')
    print(g_fixed_list)
    return g_fixed_list


def gen_new_jsa(jsa_fname, fixed_jsa):
    file_name = 'jsa.xlsx'
    wb = load_workbook(filename=file_name)
    # sheet_ranges = wb['Sheet']
    # print(sheet_ranges['A1'].value) #获取A1的值
    ws = wb['Sheet']  # 根据Sheet1这个sheet名字来获取该sheet
    ws["Q1"] = 'Fixed-state'  # 修改A1的值为LJK5679842
    rows_iter = iter(ws.rows)
    next(rows_iter) # skip line 1
    for row in rows_iter:
        res = re.search(r'JSA\d+', row[2].value)
        try:
            jsa_id = res.group()
        except AttributeError:  # 正则取不到值
            print('get jsa_id error {}'.format(row[2].value))
        if jsa_id in fixed_jsa:
            row[16].value = 'fixed'
    wb.save(jsa_fname)  # 保存修改后的excel

def gen_new_tsb(tsb_fname, fixed_tsb):
    file_name = 'tsb_with_tag.xlsx'
    wb = load_workbook(filename=file_name)
    # sheet_ranges = wb['Sheet']
    # print(sheet_ranges['A1'].value) #获取A1的值
    ws = wb['sheet']  # 根据Sheet1这个sheet名字来获取该sheet
    ws["L1"] = 'Fixed-state'  # 修改A1的值为LJK5679842
    rows_iter = iter(ws.rows)
    next(rows_iter)
    for row in rows_iter:
        res = re.search(r'TSB\d+', row[3].value)
        try:
            tsb_id = res.group()
        except AttributeError:  # 正则取不到值
            print('get jsa_id error {}'.format(row[3].value))
        if tsb_id in fixed_tsb:
            row[11].value = 'fixed'
    wb.save(tsb_fname)  # 保存修改后的excel

@login_check
def handle_jsa(usrid, password):
    jsa_table = get_jsa_mapping_list() # dict
    tracking_prs = list()
    for jsa_num, prs in jsa_table.items():
        tracking_prs = tracking_prs + prs # list + list
    print('There are {} JSAs, {} tracking PRs'.format(len(jsa_table), len(tracking_prs)))
    stime = time.time()
    tracking_prs_fixed_list = jsa_check(usrid, password, tracking_prs=tracking_prs)
    fixed_jsa = list()
    for jsa in jsa_table:
        d = [False for pr in jsa_table[jsa] if pr not in tracking_prs_fixed_list]
        # print('*******************************************************************')
        if d:
            print('{} is not-fixed'.format(jsa))
            # print(d)
            # not fixed
        else:
            # print(d)
            print('{} is fixed'.format(jsa)) # fixed
            fixed_jsa.append(jsa)
    jsa_fname = 'New_JSA{}.xlsx'.format(get_time())
    gen_new_jsa(jsa_fname, fixed_jsa)
    # 下面代码debug时（check excel中fix的jsa和真正fix的jsa数量的时候）再用
    # jsa_txt_fname = 'JSA_check_result_{}.txt'.format(get_time())
    # with open(jsa_txt_fname, 'a') as fobj:
    #     for i in fixed_jsa:
    #         fobj.write(i + '\n')
    etime = time.time()
    print('Done, {} JSAs takes {}s'.format(len(jsa_table), etime - stime))
    print('There are {} JSAs that you can exclude'.format(len(fixed_jsa)))

@login_check
def handle_tsb(usrid, password):
    tsb_table = get_tsb_mapping_list() # dict
    # print(tsb_table)
    tracking_prs = list()
    for jsa_num, prs in tsb_table.items():
        tracking_prs = tracking_prs + prs # list
    print('There are {} TSBs, {} tracking PRs'.format(len(tsb_table), len(tracking_prs)))
    stime = time.time()
    tracking_prs_fixed_list = tsb_check(usrid, password, tracking_prs=tracking_prs)
    fixed_tsb = list()
    for tsb in tsb_table:
        d = [False for pr in tsb_table[tsb] if pr not in tracking_prs_fixed_list]
        # print('*******************************************************************')
        if d:
            print('{} is not-fixed'.format(tsb))
            # print(d)
            # not fixed
        else:
            # print(d)
            print('{} is fixed'.format(tsb))  # fixed
            fixed_tsb.append(tsb)
    tsb_fname = 'TSB_check_result_{}.xlsx'.format(get_time())
    gen_new_tsb(tsb_fname, fixed_tsb)
    tsb_txt_fname = 'TSB_check_result_{}.txt'.format(get_time())
    with open(tsb_txt_fname, 'a') as fobj:
        for i in fixed_tsb:
            fobj.write(i + '\n')

    etime = time.time()
    print('Done, {} TSBs takes {}s'.format(len(tsb_table), etime - stime))
    print('There are {} TSBs that you can exclude'.format(len(fixed_tsb)))

@login_check
def svn_link_check(usrid, password):
    requests.packages.urllib3.disable_warnings()
    url = input("Please provide URL for SVN Check result:")
    """
        第一次请求svnmap,获取cookie PHPSESSID
    """
    print('Please wait.....')
    stime = time.time()
    header = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0"}
    Data = requests.get(url, headers=header, verify=False)
    PHPSession_ID = Data.cookies["PHPSESSID"]
    Keyword = re.compile('name="SAMLRequest" value="(.*?)"')
    SAMLRequest = Keyword.findall(Data.text)
    '''
    第二步向https://iam-sso.juniper.net/oamfed/idp/samlv20提交SAMLRequest
    '''
    url2 = "https://iam-sso.juniper.net/oamfed/idp/samlv20"
    header2 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url}
    Data2 = requests.post(url2, headers=header2, verify=False, data={"SAMLRequest": SAMLRequest, "RelaySate": url})
    cookies = requests.utils.dict_from_cookiejar(Data2.history[0].cookies)
    Updatecookies = requests.utils.dict_from_cookiejar(Data2.cookies)
    cookies.update(Updatecookies)

    """
    第三步向https://iam-sso.juniper.net/oam/server/auth_cred_submit发起POST请求
    """
    url3 = 'https://iam-sso.juniper.net/oam/server/auth_cred_submit'
    header3 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": Data2.url}
    Data3 = requests.post(url3, headers=header3, verify=False,
                          data={"HiddenURI": None, "LOCALE": "en_us", "AUTHMETHOD": "UserPassword", "userid": usrid,
                                "password": password, "rememberme": "on"}, cookies=cookies)
    Keyword = re.compile('NAME="SAMLResponse" VALUE="(.*)"/>', re.DOTALL)
    SAMLResponse = Keyword.findall(Data3.text)
    """
    第四步向https://gubber.juniper.net/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp发起POST请求
    """
    url4 = 'https://gubber.juniper.net/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp'
    header4 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url3}
    Data4 = requests.post(url4, headers=header4, verify=False,
                          data={"SAMLResponse": SAMLResponse[0], "RelayState": url},
                          cookies={"PHPSESSID": PHPSession_ID})

    """
    获取所有结果
    """
    # from lxml import etree
    # tree = etree.HTML(Data4.content)
    # pr = tree.xpath('//*[@id="svn-map"]/div[2]/div/table/tbody/tr/td[1]/a')
    # res = tree.xpath('// *[ @ id = "result"]')
    # print(pr, res)

    from bs4 import BeautifulSoup
    soup = BeautifulSoup(Data4.text, "html.parser")
    tables = soup.find("table", "bordered")
    sections = tables.find_all('tr')
    result_dict = dict()
    for i in range(1, len(sections)):
        git_says = ''
        new_B = sections[i].select('#result')
        # print(sections[i].select('#result'))
        # print(type)
        for item in new_B:
            # print(type(item))
            # print(item.getText())
            git_says += item.getText()

        # print(git_says)
        # try:
        #     git_says = sections[i].b.font.getText()
        # except:
        #     git_says = ''
        record = sections[i].td.getText() + sections[i].b.getText() + git_says
        # if '1320374' in record:
        #     print(record)
        if 'YES' in record and 'CAUTION' not in record:
            Records = re.compile('^PR (\d+)').findall(record)
            # print(Records)
            result_dict[Records[0]] = 'YES'

    """
    提取所有SVN-Says： 
    """
    svn_link_fname = 'SVN_Fetch_result_{}.txt'.format(get_time())
    with open(svn_link_fname, 'a') as fobj:
        for key in result_dict:
            fobj.write(key + '\n')
    print('There are {} fixed PRs, Please see in {} '.format(len(result_dict), svn_link_fname))
    etime = time.time()
    print('It takes {} s'.format(etime-stime) )

    return result_dict

def menu():
    while 1:
        promt = '''******* help doc for SVN automation tool *******
           Please choose function as below, input the fuction num: 
           1) SVN High Speed Check 
           2) JSA Exclude
           3) TSB Exclude
           4) SVN Result link check
           5) Version info
           6) quit

           Your choice: 
           '''
        choice = input(promt).strip()
        cmd = {'1': bulk_check, '2': handle_jsa, '3': handle_tsb, '4': svn_link_check}
        if choice.strip() == '1':
            cmd['1']()
        elif choice.strip() == '2':
            cmd['2']()
        elif choice.strip() == '3':
            cmd['3']()
        elif choice.strip() == '4':
            cmd['4']()
        elif choice.strip() == '5':
            version_promt = '''
            Created on Tue Apr 23 22:39:43 2019
            @author: Neal
            Version: 1.0   Apr 23
            Version: 1.1   May 21
            Version: 1.2   May 31
            Version: 1.3   Jun 2
            '''
            time.sleep(0.5)
            print(version_promt)
        elif choice.strip() == '6':
            print('Bye ~')
            break
        else:
            print('Invalid Input, automatic return to menu...')
            time.sleep(0.5)


if __name__ == '__main__':
    try:
        freeze_support()
        menu()
    except KeyboardInterrupt:
        print('')
        print('Bye~Bye')
        time.sleep(1)

