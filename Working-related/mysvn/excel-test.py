from openpyxl import load_workbook
file_home = 'jsa.xlsx'
wb = load_workbook(filename= file_home)
sheet_ranges = wb['Sheet']
print(sheet_ranges['A1'].value)
ws = wb['Sheet'] #根据Sheet1这个sheet名字来获取该sheet
# ws["A1"] = 'LJK5679842'   #修改A1的值为LJK5679842
# ws['D6'] = 'LKJI66666666666666'  #修改D6的值为，其中D6-F6是合并后的单元格，修改数据需修改最左上角单元格数据，来替代整个merge单元格
ws['Q1'] = 'fixed-state'
rows_iter = iter(ws.rows)
next(rows_iter)
for row in rows_iter:
    row[16].value = 'fixed'
wb.save('jsa-ok4.xlsx')    #保存修改后的excel
