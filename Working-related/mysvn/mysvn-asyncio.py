# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 10:57:26 2019

@author: nealhuo
"""
import requests
import re
from lxml import etree
import time
from openpyxl import load_workbook
import getpass
# from concurrent.futures import ThreadPoolExecutor
import asyncio


userinfo = dict()
fixed_list = list()
fixed_list_jsa_prs = list()
fixed_list_tsb_prs = list()
congestion_list = list()


def login_check(func):
    '''
    @login_check   # login_check(bulk_check)() = inner()
    def bulk_check()
    '''
    def inner():
        if userinfo:
            func(userinfo['userid'], userinfo['password'])
        else:
            userid = input('Please Enter your windows account: ')
            userid = userid.strip()
            password = getpass.getpass()
            password = password.strip()
            userinfo['userid'] = userid
            userinfo['password'] = password
            func(userinfo['userid'], userinfo['password'])

    return inner





def get_time():
    fmat = '%Y_%m_%d_%H_%M_%S'
    value = time.localtime(int(time.time()))
    dt = time.strftime(fmat, value)
    return dt

@asyncio.coroutine
def single_check(url, usrid, password, bug_id, version):
    requests.packages.urllib3.disable_warnings()
    header = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0"}
    Data = yield from requests.get(url, headers=header, verify=False)
    PHPSession_ID = Data.cookies["PHPSESSID"]
    # print(Data.cookies['PHPSESSID']) # 获取session
    Keyword = re.compile('name="SAMLRequest" value="(.*?)"')  # (.*?)才是re表达式
    SAMLRequest = Keyword.findall(Data.text)  # 取出session
    # print(SAMLRequest)
    '''
        第二步向https://iam-sso.juniper.net/oamfed/idp/samlv20提交SAMLRequest
    '''
    url2 = "https://iam-sso.juniper.net/oamfed/idp/samlv20"
    header2 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url}
    Data2 = yield from requests.post(url2, headers=header2, verify=False, data={"SAMLRequest": SAMLRequest, "RelaySate": url})
    cookies = requests.utils.dict_from_cookiejar(Data2.history[0].cookies)
    Updatecookies = requests.utils.dict_from_cookiejar(Data2.cookies)  # 将cookiejar转换为字典
    cookies.update(Updatecookies)  # 更新url2的cookie

    """
        第三步向https://iam-sso.juniper.net/oam/server/auth_cred_submit发起POST请求用于登录
    """
    url3 = 'https://iam-sso.juniper.net/oam/server/auth_cred_submit'
    header3 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": Data2.url}
    Data3 = yield from requests.post(url3, headers=header3, verify=False,
                          data={"HiddenURI": None, "LOCALE": "en_us", "AUTHMETHOD": "UserPassword", "userid": usrid,
                                "password": password, "rememberme": "on"}, cookies=cookies)
    Keyword = re.compile('NAME="SAMLResponse" VALUE="(.*)"/>', re.DOTALL)
    SAMLResponse = Keyword.findall(Data3.text)
    """
        第四步向https://gubber.juniper.net/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp发起POST请求
    """
    url4 = 'https://gubber.juniper.net/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp'
    header4 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url3}
    Data4 = yield from requests.post(url4, headers=header4, verify=False,
                          data={"SAMLResponse": SAMLResponse[0], "RelayState": url},
                          cookies={"PHPSESSID": PHPSession_ID})
    AuthToken = Data4.history[0].cookies["SimpleSAMLAuthToken"]

    url5 = 'https://gubber.juniper.net/svn-map/is_it_fixed.php?repos=&bug_id={}&branch_name={}'.format(bug_id, version)
    header5 = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
               "Referer": url}
    resp = yield from requests.get(url5, headers=header5,
                        verify=False,
                        cookies={"PHPSESSID": PHPSession_ID, "SimpleSAMLAuthToken": AuthToken}
                        )

    content = resp.text
    html = etree.HTML(content)
    html_data = html.xpath('//*[@id="result"]/font')
    caution = html.xpath('//*[@id="result"]/b[1]/font')

    html_data_list = list(html_data)

    if (len(html_data_list)) == 0:  # TODO Fixed it!!!
        print('{}: network congestion, will try manually again..'.format(bug_id))
        # for i in range(3):   #TODO bug fixed
        #     resp = requests.get(url5, headers=header5,
        #                         verify=False,
        #                         cookies={"PHPSESSID": PHPSession_ID, "SimpleSAMLAuthToken": AuthToken}
        #                         )
        #
        #     content = resp.text
        #     html = etree.HTML(content)
        #     html_data = html.xpath('//*[@id="result"]/font')
        #     caution = html.xpath('//*[@id="result"]/b[1]/font')
        #     html_data_list = list(html_data)
        #     print("{}'s {} try, the length is {} ".format(bug_id, i, len(html_data_list)))
        #
        #     if (len(html_data_list)) != 0:
        #         print('{}'.format(bug_id), len(html_data_list))
        #         break

    for text in caution:
        if 'CAUTION' in text.text:
            print('pr {} is Caution pr!!!'.format(bug_id))
            return None
    for r in html_data_list:
        if r.text:  # r.text maybe equal to None
            if r.text.strip() == 'YES':
                if bug_id not in fixed_list:
                    fixed_list.append(bug_id)
                    print('{}: fixed'.format(bug_id))
                    break

    if bug_id not in fixed_list:
        print('{}: not-fixed'.format(bug_id))

    return None


@login_check
def bulk_check(usrid, password):
    url = 'https://gubber.juniper.net/svn-map/'
    version = input('Please enter version: ')
    version = version.strip()
    print('Please enter PRs Number, Enter q when you are finished')
    bug_ids = list()
    while 1:
        bug_id = input()
        if bug_id.strip() == 'q':
            break
        bug_ids.append(bug_id)
    # print('Starting to check......')
    stime = time.time()
    result_fname = 'Bcheck_result_{}.txt'.format(get_time())

    print('starting to high-speed-check. There are {} PRs'.format(len(bug_ids)))
    task = [single_check(url, usrid, password, bug_id, version) for bug_id in bug_ids]
    # 获取一个事件循环对象
    loop = asyncio.get_event_loop()
    # 在事件循环中执行task列表
    loop.run_until_complete(asyncio.wait(task))
    loop.close()

    etime = time.time()
    write_list = list()
    for pr in fixed_list:
        write_list.append(pr + '\n')
    with open(result_fname, 'a') as robj:
        robj.writelines(write_list)
    # print('fixed_list: {}'.format(fixed_list))
    print('Done, {} PRs takes {}s'.format(len(bug_ids), etime - stime))
    print('Of the {} PRs, {} have been fixed, Please see in log for detail'.format(len(bug_ids), len(fixed_list)))


def get_jsa_mapping_list():
    # 默认可读写，若有需要可以指定write_only和read_only为True
    try:
        wb = load_workbook('jsa.xlsx')
    except FileNotFoundError as info:
        print('The "jsa.xlsx" cannot be found in the directory, please confirm it. ')
        menu()
    # 根据sheet名字获得sheet
    a_sheet = wb.get_sheet_by_name('Sheet')
    mapping_table = dict()
    for row in a_sheet.rows:
        if row[11].value:
            tracking_pr = row[11].value.split(',')
            res = re.search(r'JSA\d+', row[2].value)
            try:
                jsa_id = res.group()
                mapping_table[jsa_id] = tracking_pr
            except AttributeError:  # 正则取不到值
                print(row[2].value)
    return mapping_table


def get_tsb_mapping_list():
    # 默认可读写，若有需要可以指定write_only和read_only为True
    try:
        wb = load_workbook('tsb_with_tag.xlsx')
    except FileNotFoundError as info:
        print(
            'The "tsb_with_tag.xlsx" cannot be found in the directory, please confirm it. if the format is xls, It needs to be saved as xlsx. ')
        menu()
    # 根据sheet名字获得sheet
    a_sheet = wb.get_sheet_by_name('sheet')
    mapping_table = dict()
    for row in a_sheet.rows:
        v = row[10].value
        if v != ' ' and v != None:
            tracking_pr = row[10].value.split(',')
            res = re.search(r'TSB\d+', row[3].value)
            try:
                jsa_id = res.group()
                mapping_table[jsa_id] = tracking_pr
            except AttributeError:  # 正则取不到值
                print(row[3].value)
    return mapping_table


def jsa_check(usrid, password, tracking_prs):
    url = 'https://gubber.juniper.net/svn-map/'
    version = input('Please enter version: ')
    version = version.strip()
    bug_ids = tracking_prs
    print('Starting to JSA check......')
    pool = ThreadPoolExecutor(15)
    for bug_id in bug_ids:
        # print('starting to check pr {}'.format(bug_id))
        pool.submit(single_check, url, usrid, password, bug_id, version)
    pool.shutdown(wait=True)
    # print('done, fixed_list is {}'.format(fixed_list))
    return None


def tsb_check(usrid, password, tracking_prs):
    url = 'https://gubber.juniper.net/svn-map/'
    version = input('Please enter version: ')
    version = version.strip()
    bug_ids = tracking_prs
    print('Starting to TSB check......')
    pool = ThreadPoolExecutor(20)
    for bug_id in bug_ids:
        # print('starting to check pr {}'.format(bug_id))
        pool.submit(single_check, url, usrid, password, bug_id, version)
    pool.shutdown(wait=True)
    return None


@login_check
def handle_jsa(usrid, password):
    jsa_table = get_jsa_mapping_list()  # dict
    tracking_prs = list()
    for jsa_num, prs in jsa_table.items():
        tracking_prs = tracking_prs + prs  # list + list
    print('There are {} JSAs, {} tracking PRs'.format(len(jsa_table), len(tracking_prs)))
    stime = time.time()
    global fixed_list
    # print(fixed_list)
    if not fixed_list:
        fixed_list = []
    jsa_check(usrid, password, tracking_prs=tracking_prs)  # TODO need to clean fixed_list
    # print(fixed_list)
    fixed_jsa = list()
    for jsa in jsa_table:
        d = [False for pr in jsa_table[jsa] if pr not in fixed_list]
        # print('*******************************************************************')
        if d:
            print('{} is not-fixed'.format(jsa))
            # print(d)
            # not fixed
        else:
            # print(d)
            print('{} is fixed'.format(jsa))  # fixed
            fixed_jsa.append(jsa + '\n')
    jsa_fname = 'JSA_check_result_{}.txt'.format(get_time())
    with open(jsa_fname, 'a') as robj:
        robj.writelines(fixed_jsa)
    etime = time.time()
    print('Done, {} JSAs takes {}s'.format(len(jsa_table), etime - stime))
    print('There are {} JSAs that you can exclude'.format(len(fixed_jsa)))


@login_check
def handle_tsb(usrid, password):
    tsb_table = get_tsb_mapping_list()  # dict
    # print(tsb_table)
    tracking_prs = list()
    for jsa_num, prs in tsb_table.items():
        tracking_prs = tracking_prs + prs  # list
    print('There are {} TSBs, {} tracking PRs'.format(len(tsb_table), len(tracking_prs)))
    stime = time.time()
    global fixed_list
    if not fixed_list:
        fixed_list = []
    tsb_check(usrid, password, tracking_prs=tracking_prs)
    fixed_tsb = list()
    for tsb in tsb_table:
        d = [False for pr in tsb_table[tsb] if pr not in fixed_list]
        # print('*******************************************************************')
        if d:
            print('{} is not-fixed'.format(tsb))
            # print(d)
            # not fixed
        else:
            # print(d)
            print('{} is fixed'.format(tsb))  # fixed
            fixed_tsb.append(tsb + '\n')
    jsa_fname = 'TSB_check_result_{}.txt'.format(get_time())
    with open(jsa_fname, 'a') as robj:
        robj.writelines(fixed_tsb)
    etime = time.time()
    print('Done, {} TSBs takes {}s'.format(len(tsb_table), etime - stime))
    print('There are {} TSBs that you can exclude'.format(len(fixed_tsb)))


def scope_check():
    pass


def menu():
    while 1:
        promt = '''******* help doc for SVN automation tool *******
           Please choose function as below, input the fuction num: 
           1) SVN High Speed Check 
           2) JSA Exculde
           3) TSB Exculde
           4) Scope Check( for NS)
           5) Version info
           6) quit

           Your choice: 
           '''
        choice = input(promt).strip()
        cmd = {'1': bulk_check, '2': handle_jsa, '3': handle_tsb, '4': scope_check}
        if choice.strip() == '1':
            cmd['1']()
        elif choice.strip() == '2':
            cmd['2']()
        elif choice.strip() == '3':
            cmd['3']()
        elif choice.strip() == '4':
            cmd['4']()
        elif choice.strip() == '5':
            version_promt = '''
            Created on Tue Apr 23 22:39:43 2019
            @author: nealhuo
            Version: 1.0

            Last Update: 5/8/2019
            Version: 1.1
            '''
            time.sleep(0.5)
            print(version_promt)
        elif choice.strip() == '6':
            print('Bye ~')
            break
        else:
            print('Invalid Input, automatic return to menu...')
            time.sleep(0.5)


if __name__ == '__main__':
    try:
        menu()
    except KeyboardInterrupt:
        print('')
        print('Bye~Bye')
        time.sleep(0.5)

