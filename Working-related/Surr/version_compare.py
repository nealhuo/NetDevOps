# -*- coding: utf-8 -*-

import xlrd
import re
from xlutils.copy import copy
import time


def log(*args, **kwargs):
    fmat = '%Y/%m/%d %H:%M:%S'
    value = time.localtime(int(time.time()))
    dt = time.strftime(fmat, value)
    print('*******************Log******************', '\n', dt, '\n', *args, **kwargs)
    

def version_compare(feature, template, new_version, old_version):
    new_version_excel_data = xlrd.open_workbook(feature)  #打开feature
    new_version_excel_sheet = new_version_excel_data.sheet_by_name("Sheet1")
    new_version_excel_rows = new_version_excel_sheet.nrows

    template_excel_data = xlrd.open_workbook(template, formatting_info=True)  #打开template
    template_excel_sheet = template_excel_data.sheet_by_name("Sheet1")
    template_excel_rows = template_excel_sheet.nrows

    long_dash_unicode = u"\u2014"  # str '-' in contents
    long_dash_utf8 = long_dash_unicode.encode('utf-8')

    template_dict = {}  # 用于存储template内容
    repeat_list = []  # 用于存储重复项
    bas_version_list = []

    for temp_row in range(1, template_excel_rows):
            bas_version_value = template_excel_sheet.cell(temp_row, 1).value
            bas_version_list.append(bas_version_value)
            if bas_version_value == old_version:
                all_row_temp = template_excel_sheet.row_values(temp_row)
                bas_cont_value = template_excel_sheet.cell(temp_row, 2).value
                content = re.sub(long_dash_utf8.decode('utf-8') + ".*", "", bas_cont_value.replace("\n", ""))
                content = content.strip()
                if content in template_dict:
                    template_dict.pop(content)
                    repeat_list.append(content)
                else:
                    template_dict[content] = all_row_temp
                    # to solve the problems: the same key will be replaced in dict
            else:
                pass

    if old_version not in bas_version_list:
        raise RuntimeError('old version does not exist in template, please check!!!')
    #print ("There are %s unique key words in template in %s" % (len(template_dict), old_version))


    newFeature_dict = {}
    # newFeature_repeat_dict = {}
    for new_row in range(1, new_version_excel_rows):
        version_value = new_version_excel_sheet.cell(new_row, 1).value

        if version_value == new_version:
            all_row_new = new_version_excel_sheet.row_values(new_row)
            cont_value = all_row_new[2]
            content = re.sub(long_dash_utf8.decode('utf-8') + ".*", "", cont_value.replace("\n", ""))
            content = content.strip()
            if content in newFeature_dict:
                newFeature_dict.pop(content)
                # newFeature_repeat_dict[content] = all_row_new
            else:
                newFeature_dict[content] = all_row_new


    #print "There are %s unique key words in derek's feature in %s" % (len(newFeature_dict),new_version)


    new_obj = copy(template_excel_data)
    for key in list(newFeature_dict):  # write intersection data
        if key in template_dict:
            template_dict[key][1] = new_version
            for lines in range(template_excel_rows, template_excel_rows + 1):
                for i in range(19):
                    try:
                        new_obj.get_sheet(0).write(lines, i, template_dict[key][i])
                    except:
                        pass
                new_obj.get_sheet(0).write(lines, 2, newFeature_dict[key][2]) #bug-fix code.
                template_excel_rows += 1
            newFeature_dict.pop(key)
        else:
            pass

    for key in newFeature_dict:
        for lines in range(template_excel_rows, template_excel_rows + 1):
            for i in range(5):
                new_obj.get_sheet(0).write(lines, i, newFeature_dict[key][i])
            template_excel_rows += 1
    print ('There are %s items need to review one by one:' % len(newFeature_dict))
    print ()
    new_obj.save('book-%s.xls' % new_version)



def find_repeat():
    pass


def flash_single_version(version, feature, template):
    #打开模板
    template_excel_data = xlrd.open_workbook(template, formatting_info=True)
    template_excel_sheet = template_excel_data.sheet_by_name("Sheet1")
    template_excel_rows = template_excel_sheet.nrows

    #打开new feature
    feature_excel_data = xlrd.open_workbook(feature)
    feature_excel_sheet = feature_excel_data.sheet_by_name("Sheet1")
    feature_excel_rows = feature_excel_sheet.nrows

    long_dash_unicode = u"\u2014"  # str '-' in contents
    long_dash_utf8 = long_dash_unicode.encode('utf-8')

    #遍历template,生成字典，打印出重复项
    bas_version_dict = {}
    template_dict = {}
    template_repeat_list = []
    for temp_row in range(1, template_excel_rows):
        bas_version_value = template_excel_sheet.cell(temp_row, 1).value
        bas_version_dict[bas_version_value] = ''
        if bas_version_value == version:
            all_row_temp = template_excel_sheet.row_values(temp_row)
            bas_cont_value = template_excel_sheet.cell(temp_row, 2).value
            content = re.sub(long_dash_utf8.decode('utf-8') + ".*", "", bas_cont_value.replace("\n", ""))
            content = content.strip()
            if content in list(template_dict):
                template_repeat_list.append(content)
                template_dict.pop(content)
            else:
                template_dict[content] = all_row_temp

    # 遍历feature
    feature_dict = {}
    feature_repeat_list = []
    fea_version_dict = {}
    for fea_row in range(1, feature_excel_rows):
        fea_version_value = feature_excel_sheet.cell(fea_row, 1).value
        fea_version_dict[fea_version_value] = ''
        if fea_version_value == version:
            all_row_new = feature_excel_sheet.row_values(fea_row)
            cont_value = all_row_new[2]
            content = re.sub(long_dash_utf8.decode('utf-8') + ".*", "", cont_value.replace("\n", ""))
            content = content.strip()
            if content in feature_dict:
                feature_repeat_list.append(content)
            else:
                feature_dict[content] = all_row_new
        else:
            pass


    new_obj = copy(template_excel_data)
    
    #写入新的content
    for line in range(template_excel_rows):
        temp_version_value = template_excel_sheet.cell(line, 1).value
        if temp_version_value == version:

            # new_obj.get_sheet(0).write(line, 2, 'need review one by one')
            cont_value = template_excel_sheet.cell(line, 2).value
            key = re.sub(long_dash_utf8.decode('utf-8') + ".*", "", cont_value.replace("\n", ""))
            key = key.strip()
            if key in feature_dict:
                new_obj.get_sheet(0).write(line, 2, feature_dict[key][2])

            else:
                if key in feature_repeat_list or key in template_repeat_list:
                    new_obj.get_sheet(0).write(line, 19, 'need review one by one')
    new_obj.save('book-%s.xls' % version)
    #print 'congratulation......done'


def get_temp_version(template):
    template_excel_data = xlrd.open_workbook(template, formatting_info=True)  #打开template
    template_excel_sheet = template_excel_data.sheet_by_name("Sheet1")
    template_excel_rows = template_excel_sheet.nrows
    
    #遍历template,获取version list
    bas_version_dict = {}
    for temp_row in range(1, template_excel_rows):
        bas_version_value = template_excel_sheet.cell(temp_row, 1).value
        bas_version_dict[bas_version_value] = ''
        
    temp_version = list(bas_version_dict)
    
    return temp_version
        
def deal_excel(version, feature, template, new_obj):
    #打开模板
    template_excel_data = xlrd.open_workbook(template, formatting_info=True)
    template_excel_sheet = template_excel_data.sheet_by_name("Sheet1")
    template_excel_rows = template_excel_sheet.nrows

    #打开new feature
    feature_excel_data = xlrd.open_workbook(feature)
    feature_excel_sheet = feature_excel_data.sheet_by_name("Sheet1")
    feature_excel_rows = feature_excel_sheet.nrows

    long_dash_unicode = u"\u2014"  # str '-' in contents
    long_dash_utf8 = long_dash_unicode.encode('utf-8')

    #遍历template,生成字典，打印出重复项
    bas_version_dict = {}
    template_dict = {}
    template_repeat_list = []
    for temp_row in range(1, template_excel_rows):
        bas_version_value = template_excel_sheet.cell(temp_row, 1).value
        bas_version_dict[bas_version_value] = ''
        if bas_version_value == version:
            all_row_temp = template_excel_sheet.row_values(temp_row)
            bas_cont_value = template_excel_sheet.cell(temp_row, 2).value
            content = re.sub(long_dash_utf8.decode('utf-8') + ".*", "", bas_cont_value.replace("\n", ""))
            content = content.strip()
            if content in list(template_dict):
                template_repeat_list.append(content)
                template_dict.pop(content)
            else:
                template_dict[content] = all_row_temp
    
    # 遍历feature
    feature_dict = {}
    feature_repeat_list = []
    fea_version_dict = {}
    for fea_row in range(1, feature_excel_rows):
        fea_version_value = feature_excel_sheet.cell(fea_row, 1).value
        fea_version_dict[fea_version_value] = ''
        if fea_version_value == version:
            all_row_new = feature_excel_sheet.row_values(fea_row)
            cont_value = all_row_new[2]
            content = re.sub(long_dash_utf8.decode('utf-8') + ".*", "", cont_value.replace("\n", ""))
            content = content.strip()
            if content in feature_dict:
                feature_repeat_list.append(content)
            else:
                feature_dict[content] = all_row_new
        else:
            pass


    #new_obj = copy(template_excel_data)
    
    #写入新的content
    for line in range(template_excel_rows):
        temp_version_value = template_excel_sheet.cell(line, 1).value
        if temp_version_value == version:

            # new_obj.get_sheet(0).write(line, 2, 'need review one by one')
            cont_value = template_excel_sheet.cell(line, 2).value
            key = re.sub(long_dash_utf8.decode('utf-8') + ".*", "", cont_value.replace("\n", ""))
            key = key.strip()
            if key in feature_dict:
                new_obj.get_sheet(0).write(line, 2, feature_dict[key][2])

            else:
                if key in feature_repeat_list or key in template_repeat_list:
                    new_obj.get_sheet(0).write(line, 19, 'need review one by one')
    
    return new_obj             


def flash_all_version(feature, template):
    
    temp_version_list = get_temp_version(template)
    
    #打开模板
    template_excel_data = xlrd.open_workbook(template, formatting_info=True)    
    new_obj = copy(template_excel_data)
    
    for temp_version in temp_version_list:
        new_obj = deal_excel(temp_version, feature, template, new_obj)
        log('{} is finished'.format(temp_version))
        
        
    new_obj.save('flash_result.xls')


def help():
    pass



if __name__ == '__main__':
    promt = '''******* welcome to use SURR template maintain tool *******
        Please choose function as below, input the fuction num: 
        1) version compare (Add new version to compare old version, example, 16.1R7 compare with 16.1R6)
        2) reverse version compare (example, 16.1R6 compare with 16.1R7)
        3) find the repeat item in template
        4) find the repeat item in Derek's Feature
        5) flash the single version template content from Derek's Feature 
        6) flash the all version template content from Derek's Feature
        7) Quit
    
        Your choice: 
        '''
    cmd = {'1': version_compare, '2': 'reverse_version_compare', '3': 'find_repeat_template',
           '4': 'find_repeat_feature', '5': flash_single_version, '6': flash_all_version}
    choice = str(input(promt)).strip()

    if choice == '1':
        version = input('Please input the new version and old version (example: 16.1R2 16.1R1): ')
        new_version = version.split()[0]
        old_version = version.split()[1]
        cmd['1'](feature='Behavior.xls', template='template.xls', new_version=new_version, old_version=old_version)
    elif choice == '2':
        pass
    elif choice == '3':
        pass
    elif choice == '4':
        pass
    elif choice == '5':
        version = input('Which version do you want to flash ? ')
        cmd['5'](version, 'Behavior.xls', 'template.xls')
        
    elif choice == '6':
        cmd['6']('Behavior.xls', 'template.xls')
        
    elif choice == '7':
        print ('Quit')
        quit()
    else:
        print ('ERROR INPUT')

    print ("done")






