import requests
import time
import random
from requests.auth import HTTPBasicAuth
from lxml import etree
from multiprocessing import Manager, Pool

ua_pool = [
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36 OPR/26.0.1656.60',
            'Opera/8.0 (Windows NT 5.1; U; en)',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2 ',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
            'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.133 Safari/534.16',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.11 TaoBrowser/2.0 Safari/536.11',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.71 Safari/537.1 LBBROWSER',
            'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E)',
            'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 SE 2.X MetaSr 1.0',
            ]

def get_time():
    fmat = '%Y_%m_%d_%H_%M_%S'
    value = time.localtime(int(time.time()))
    dt = time.strftime(fmat, value)
    return dt

def check_scope(pr, sumlist):
    url = 'https://gnats.juniper.net/web/default/{}#scope_tab'.format(pr)
    headers = {'User-Agent': random.choice(ua_pool)}
    r = requests.get(url, headers=headers, auth=HTTPBasicAuth('nealhuo', 'Feifei4569@zhanbo'))
    html = etree.HTML(r.content)
    l = html.xpath('//*[@id="sctab_identifier"]/thead/tr/th')
    print(len(l))
    if len(l) == 2:
        sumlist.append(pr)
    return None


if __name__ == '__main__':
    result_fname = 'result_{}.txt'.format(get_time())
    pool = Pool(50)
    with open('PRs.txt') as fobj:
        with Manager() as manager:
            sumlist = manager.list()
            for pr in fobj:
                t = pool.apply_async(func=check_scope, args=(pr, sumlist))
            pool.close()
            pool.join()
            with open(result_fname, 'a') as robj:
                robj.writelines(sumlist)






