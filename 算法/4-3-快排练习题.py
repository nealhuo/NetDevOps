# 返回列表中的最大数

def max(list):
    m = list[0]
    for i in list:
        if i > m:
            m = i
    return m

def max2(list):
    if len(list) == 2:
        if list[0] > list[1]:
            return list[0]
        else:
            return list[1]
    sub_max = max2(list[1:])
    if list[0] > sub_max:
        return list[0]
    else:
        return sub_max

if __name__ == '__main__':
    print(max([1, 2, 3, 9, 7]))
    print(max2([1, 2, 3, 9, 7]))
