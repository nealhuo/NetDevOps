def merge(a, b):  # 合并
    c = []
    h = j = 0
    while j < len(a) and h < len(b):
        if a[j] < b[h]: # 左右两边的列表比大小
            c.append(a[j])
            j += 1
        else:
            c.append(b[h])
            h += 1

    if j == len(a):
        for i in b[h:]:
            c.append(i)
    else:
        for i in a[j:]:
            c.append(i)
    return c


def merge_sort(lists):  # 分开
    if len(lists) <= 1:
        return lists
    middle_index = len(lists)//2
    left = merge_sort(lists[:middle_index]) # 递归，继续分
    right = merge_sort(lists[middle_index:])
    return merge(left, right)


if __name__ == '__main__':
    a = [14, 2, 34, 43, 21, 19]
    print (merge_sort(a))

    # arr = random.sample(range(1, 1000000), 999999)
    # stime = time.time()
    # quick_sort(arr)  # 5-5.5s
    # print(time.time() - stime)
