#4.2 计算列表包含的元素数

#@time_caculate2 # time_caculate2(arr_count) = inner      time_caculate2(arr_count)() = inner()
def arr_count(arr):
    count = 0
    for i in arr:
        count = count + 1
    return count


print('用循环方法:{}'.format(arr_count([1,2,3])))

def arr_len(arr):
    try: # 太丑了！能别用异常就别用异常
        arr.pop(0)
        return 1 + arr_len(arr)
    except Exception as info:
        print(info)
        return 0

print('用递归方法:{}'.format(arr_len([1,2,3])))

# 标准答案:

def arr_len2(arr):
    if arr == []:
        return 0
    return 1 + arr_len2(arr[1:])

'''
题外话：来验证一下递归是不是比循环慢
'''
import time
arr1 = [i for i in range(10000000)]
def time_caculate(func):
    stime = time.time()
    func(arr=arr1)
    etime = time.time()
    print(etime - stime)

# def time_caculate2(func):
#     def inner():
#         stime = time.time()
#         func(arr=arr1)
#         etime = time.time()
#         return etime - stime
#     return inner

time_caculate(arr_count)   #0.49秒
time_caculate(arr_len)   # 12.23秒 ！！！

# 找出列表中的最大数字


