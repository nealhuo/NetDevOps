import time
import random


def quick_sort(arr):
    if len(arr) < 2:
        return arr
    pivot = arr[0]
    less = [i for i in arr[1:] if i < pivot]
    greater = [i for i in arr[1:] if i > pivot]
    return quick_sort(less) + [pivot] + quick_sort(greater)

def quick_sort2(arr):
    if len(arr) < 2:
        return arr
    pivot_index = len(arr) // 2
    pivot = arr[pivot_index]
    less = [i for i in arr if i < pivot]
    greater = [i for i in arr if i > pivot]
    return quick_sort(less) + [pivot] + quick_sort(greater)


if __name__ == '__main__':

    arr = random.sample(range(1, 1000000), 999999)
    stime = time.time()
    quick_sort(arr)  # 5-5.5s
    print(time.time()-stime)

    stime = time.time()
    quick_sort2(arr) #4.5-5s  可见基准值的重要性啊！！！！因为快排也属于迭代，所以速度快慢取决于调用栈的长度。
    print(time.time() - stime)

    stime = time.time()
    arr.sort() # 只要0.5s，还是内置函数效率高，特别是用C写的
    print(time.time() - stime)

    a = [14, 2, 34, 43, 21, 19]
    print(quick_sort2(arr=a))