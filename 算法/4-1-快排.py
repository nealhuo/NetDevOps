# 把列表中的元素累加起来

# 用循环方法：
def sum(arr):
    res = 0
    for i in arr:
        res = res + i
    return res

print('用循环方法:{}'.format(sum([1,2,3])))

def sum2(arr):
    if len(arr) == 1: # 基线条件
        return arr[0]
    else: # 迭代条件
        return arr.pop(0) + sum2(arr)

# 标准答案
def sum3(arr):
    if arr == []:
        return 0
    else:
        return arr[0] + sum3(arr[1:])

print('用递归方法:{}'.format(sum2([1,2,3])))



