# 目的在朋友圈中找到一位关系最近的芒果经销商，先从朋友开始找，再从朋友的朋友开始
from collections import deque

graph1 = {
    'Alice':{'Pengqi':{'LIlei':'Feifei'}, 'Candy':{'Hanmm':'Xiaoxiao'}},
          'Bob': {'Pengqi':{'LIlei':'Feifei'}, 'Huan':{'Hefei':'Tony'}},
          'John':{'Mangguo':'Tonyhan'}}

Alice_friends = [ name for name in graph1['Alice']]
Bob_friends = [name for name in graph1['Bob']]
John_friends = [name for name in graph1['John']]

def person_is_seller(person):
    if 'Mangguo' in person:
        return True
    return False

def search():
    search_queue = deque()
    search_queue += graph1  # 这里操作很灵活，可以把列表送入队列，也可以把字典的所有key当做列表送入队列
    searched = [] # 用于记录检查过的人
    while search_queue:
        person = search_queue.popleft()  # 弹出队列中第一个元素(最左边的元素)
        if person not in searched:
            if person_is_seller(person=person):
                print('{} is a mango seller!'.format(person))
                searched.append(person)
                return True
            else:
                if person in Alice_friends:
                    search_queue += graph1['Alice'][person]
                    searched.append(person)
                elif person in Bob_friends:
                    search_queue += graph1['Bob'][person]
                    searched.append(person)
                elif person in John_friends:
                    search_queue += graph1['John'][person]
                    searched.append(person)
                else:
                    search_queue += graph1[person]
                    searched.append(person)
    return False

if __name__ == '__main__':
    search()