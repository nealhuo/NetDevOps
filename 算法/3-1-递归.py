import time
def countdown(i):  # 打印3,2,1
    print(i)
    i = i-1
    if i > 0:   # 递归条件
        countdown(i)
    else:  # 基线条件
        return

def fact(x):  # 算乘结 x！
    if x == 1:  # 基线条件
        return 1
    else:  # 递归条件
        return x * fact(x-1)


if __name__ == '__main__':
    countdown(3)
    print(fact(5))




