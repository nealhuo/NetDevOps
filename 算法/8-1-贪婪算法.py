
# 需要覆盖的州
states_needed = set(['mt', 'wa','or','id','nv','ut','ca','az'])

# 每个电台覆盖的州
stations = {
    'kone':set(['id','nv','ut']),
    'ktow':set(['wa','id','mt']),
    'kthree':set(['or','nv','ca']),
    'kfour':set(['nv','ut']),
    'kfive':set(['ca','az']),
    }

# 最终选择的电台们
final_stations = set()
while states_needed:
    # 覆盖了 最多当前未覆盖的州的 电台
    best_station = None
    # 当前被电台覆盖的州
    state_covered = set()
    for station, states_for_station in stations.items():
        covered = states_needed & states_for_station  # 找到交集，也就是这个电台覆盖了哪些州
        # print(covered)
        # 贪婪算法的核心1：简单粗暴，直接判断这个电台覆盖的州的数量是否大于已覆盖的州的数量
        # 其实就是找到覆盖州最多的一个电台！
        if len(covered) > len(state_covered):
            best_station = station  # 在未覆盖的州中，覆盖州最多的电台
            state_covered = covered
        # 核心2：更新未覆盖的州，接下来while作用就是在未覆盖的州中继续找一个覆盖州最多的电台
    states_needed = states_needed - state_covered
    final_stations.add(best_station)
print(final_stations)
'''
{'kthree', 'kfive', 'ktow', 'kone'}
'''
