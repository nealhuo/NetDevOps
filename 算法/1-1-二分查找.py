'''
二分查找的时间复杂度为O(logn)
python中默认查找元素索引的方法为list.index()，这个内置方法是用C写的，是不是二分查找呢？哈哈
'''

class Binary_serach(object):
    def __init__(self):
        pass
    def binary_search_neal(self, list, item):
        l = len(list)
        mid = len(list) // 2
        while True:
            if list[mid] == item:
                return mid
            elif list[mid] > item:
                mid = mid // 2
            elif list[mid] < item:  #这里的一个嵌套降低了效率
                if mid == len(list)-1:
                    return None
                mid = (l + mid) // 2

    def binary_search_book(self, list, item):
        low = 0
        high =len(list)-1
        while low <= high:
            mid = (low+high)//2
            if list[mid] == item:
                return mid
            elif list[mid] > item:
                high = mid-1
            else:
                low = mid+1
        return None



if __name__ == '__main__':
    ordered_list = [i for i in range(9)]
    print(ordered_list)
    bs = Binary_serach()
    print(bs.binary_search_neal(ordered_list, 1))
    print(bs.binary_search_neal(ordered_list, 8))
    print(bs.binary_search_book(ordered_list, 1))
    print(bs.binary_search_book(ordered_list, 8))


