graph = {      # 拓扑图
    'start':{'a':6, 'b':2},
    'a':{'fin':1,},
    'b':{'fin':5, 'a':3},
    'fin':{},
    }
inf = float('inf')
costs = {   # 开销表
    'a':6,
    'b':2,
    'fin':inf,
    }
parents = {   # 父节点表
    'a':'start',
    'b':'start',
    'fin':None,
    }
processed = [] # 用于记录处理过的节点

def find_lowest_cost_node(costs):
    lowest_cost = float('inf')
    lowest_cost_node = None
    for node in costs:
        cost = costs[node]
        if cost < lowest_cost and node not in processed:
            lowest_cost = cost
            lowest_cost_node = node
    return lowest_cost_node

def dijastra():
    node = find_lowest_cost_node(costs=costs)  # 在未处理节点中找到距离起点开销最小的节点
    while node is not None:
        cost = costs[node]  #获得节点的cost值
        neighbors = graph[node]   # 获得这个node相邻的节点及cost的对应关系
        for n in neighbors.keys(): # 遍历当前节点的所有邻居
            new_cost = cost + neighbors[n]  # 新开销为 起点到这个node的开销 加上 这个node到邻居的开销
            if new_cost > cost:   # 如果经当前节点前往该邻居更近
                costs[n] = new_cost  # 更新开销
                parents[n] = node   # 更新父节点
        processed.append(node)  # 将当前节点标记为处理过
        node = find_lowest_cost_node(costs=costs)  # 继续找下一个节点并循环

if __name__ == '__main__':
    dijastra()
    print(costs)
    '''
    最终结果：{'a': 5, 'b': 2, 'fin': 6}
    '''
    print(parents)
    '''
    最终结果：{'a': 'b', 'b': 'start', 'fin': 'a'}
    '''