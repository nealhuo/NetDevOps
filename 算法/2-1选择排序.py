
def findsmallest(arr):
    smallest = arr[0]  # 存储最小值
    smallest_index = 0   # 存储最小值的索引
    for i in range(1, len(arr)):
        if arr[i] < smallest:
            smallest = arr[i]
            smallest_index = i
    return smallest_index, smallest

def selectionSort(arr):
    new_arr = list()
    for i in range(len(arr)):
        smallest_index, smallest = findsmallest(arr)
        new_arr.append(smallest)
        arr.pop(smallest_index)
    return new_arr

if __name__ == '__main__':
    arr = [9,5,2,3,1,4,8,6,7]
    print(selectionSort(arr=arr))
