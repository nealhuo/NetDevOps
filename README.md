# NetDevOps

#### 介绍
自动化运维-代码片段，包含基础练习，自动化脚本，工作中编写自动化软件

#### 目录结构

--Automation
	--Junos配置转换工具  #用于将junos配置转换为set格式 
	--Yaml文件处理  
	--利用paramiko下发配置
	--批量工具  # batchcmd.py
	--拓扑绘制  # 完善中...
	--设备自动发现
		--netconf方式
		--ping方式
		--snmp方式
	
--Wheels
	--fake_celery  # 根据celery写的一个异步脚本，待完善...
	--fake_django  # 基于MVC的简易web框架，待完善...
		
--Working-related  #
	--mysvn
	--NStar
	--SURR
	

--算法	

--Basic-practice # PY基础练习（装饰器/opp/redis/多线程/进程 协程 异步）

--Games  # 用于存放python编写的小游戏
	--三子棋
	

#### 安装教程

无

#### 使用说明

无

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)